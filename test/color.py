from vtk import vtkMutableDirectedGraph, vtkPoints, vtkIntArray,\
    vtkLookupTable, vtkGraphLayoutView, vtkViewTheme, vtkVariant,\
    vtkVariantArray

graph = vtkMutableDirectedGraph()

v1 = graph.AddVertex()
v2 = graph.AddVertex()
v3 = graph.AddVertex()
graph.AddGraphEdge(v1,v2)
graph.AddGraphEdge(v2,v3)

points = vtkPoints()
points.InsertNextPoint(0,0,0)
points.InsertNextPoint(1,0,0)
points.InsertNextPoint(2,0,0)

graph.SetPoints(points)

vertexColors = vtkVariantArray()
vertexColors.SetName("Color")

lookupTable = vtkLookupTable()
lookupTable.SetIndexedLookup(True) # impede de usar valores fora
lookupTable.SetNanColor(0, 0, 0, 0)
lookupTable.SetAnnotation(vtkVariant(0), 'red')
lookupTable.SetAnnotation(vtkVariant(1), 'white')
lookupTable.SetAnnotation(vtkVariant(2), 'green')

lookupTable.SetTableRange(0, 2)
lookupTable.Build()

lookupTable.SetTableValue(0, 1.0, 0.0, 0.0)
lookupTable.SetTableValue(1, 1.0, 1.0, 1.0)
lookupTable.SetTableValue(2, 0.0, 1.0, 0.0)

vertexColors.InsertNextValue(vtkVariant(1))
vertexColors.InsertNextValue(vtkVariant(2))
vertexColors.InsertNextValue(vtkVariant(2))

graph.GetVertexData().AddArray(vertexColors)

graphLayoutView = vtkGraphLayoutView()
graphLayoutView.AddRepresentationFromInput(graph)
graphLayoutView.SetLayoutStrategyToPassThrough()
graphLayoutView.SetVertexColorArrayName("Color")
graphLayoutView.ColorVerticesOn()

theme = vtkViewTheme()
theme.SetPointLookupTable(lookupTable)
theme.SetScalePointLookupTable(False) # impede a normalizacao

graphLayoutView.ApplyViewTheme(theme)
graphLayoutView.ResetCamera()
graphLayoutView.GetInteractor().Initialize()
graphLayoutView.GetRenderer().GetActiveCamera().Zoom(0.8)
graphLayoutView.GetInteractor().Start()
