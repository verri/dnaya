# -*- encoding: utf-8 -*-
# Copyright © 2014, Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.
import sys
import unittest

sys.path.append('.')
from dnaya.api import Network, NetworkVertex, NetworkEdge


class TestNetworkVertex(unittest.TestCase):

    def setUp(self):
        self.network = Network()
        self.vertex = NetworkVertex(vid=0, network=self.network)

    def test_standalone_vertex(self):
        with self.assertRaises(ValueError):
            NetworkVertex()

    def test_readonly_property_network(self):
        with self.assertRaises(AttributeError):
            self.vertex.network = None

    def test_readonly_property_id(self):
        with self.assertRaises(AttributeError):
            self.vertex.vid = 0

    def test_standalone(self):
        v = NetworkVertex(vid=0)
        self.assertEquals(v.vid, 0)

    def test_attribute(self):
        v = NetworkVertex(vid=0)
        v['weight'] = 1
        self.assertTrue('weight' in v)
        self.assertEquals(v['weight'], 1)
        self.assertEquals(v.get('weight'), 1)

    def test_attribute_default(self):
        v = NetworkVertex(vid=0)
        self.assertEquals(v.get('color'), None)
        self.assertEquals(v.get('color', 'blue'), 'blue')

    def test_attribute_error(self):
        v = NetworkVertex(vid=0)
        with self.assertRaises(KeyError):
            v['color']

    def test_edges(self):
        with self.assertRaises(ValueError):
            n = self.vertex.edges(mode='*')
        n = self.vertex.edges(mode='all')
        self.assertEquals(n, [])
        n = self.vertex.edges(mode='in')
        self.assertEquals(n, [])
        n = self.vertex.edges(mode='out')
        self.assertEquals(n, [])


class TestNetworkEdge(unittest.TestCase):

    def setUp(self):
        self.network = Network()
        self.vertex = NetworkVertex(network=self.network)
        self.vertex = NetworkVertex(network=self.network)
        self.edge = NetworkEdge(0, 1, self.network)

    def test_setitem(self):
        self.edge['item'] = 5
        self.assertEquals(self.edge['item'], 5)

    def test_contains(self):
        self.assertFalse('obj' in self.edge)
        self.edge['obj'] = 2
        self.assertTrue('obj' in self.edge)

    def test_attribute(self):
        self.edge['weight'] = 1
        self.assertTrue('weight' in self.edge)
        self.assertEquals(self.edge['weight'], 1)
        self.assertEquals(self.edge.get('weight'), 1)

    def test_attribute_default(self):
        self.assertEquals(self.edge.get('color'), None)
        self.assertEquals(self.edge.get('color', 'blue'), 'blue')

    def test_attribute_error(self):
        v = NetworkVertex(vid=0)
        with self.assertRaises(KeyError):
            v['color']


class TestNetwork(unittest.TestCase):

    def test_direction(self):
        self.assertFalse(Network(directed=False).directed)
        self.assertTrue(Network(directed=True).directed)

    def test_readonly_property_directed(self):
        network = Network()
        with self.assertRaises(AttributeError):
            network.directed = True

    def test_add_vertex(self):
        network = Network()
        self.assertIsInstance(network.add_vertex(), NetworkVertex)
        self.assertEquals(len(network.vertices),
                          network.vtk.GetNumberOfVertices())

    def test_network_data(self):
        network = Network(n=1)
        with self.assertRaises(AttributeError):
            network[0] = 1
        network['0'] = -2
        self.assertEquals(network['0'], -2)
        network['start'] = network[0]
        self.assertEquals(network['start'], network[0])

    def test_network_length(self):
        network = Network()
        self.assertEqual(len(network), 0)
        network.add_vertex()
        self.assertEqual(len(network), 1)

    def test_network_init(self):
        network = Network(n=10, directed=False)
        self.assertEqual(len(network), 10)
        network2 = Network(n=5, directed=True)
        self.assertEqual(len(network2), 5)

    def test_contains_vertex(self):
        network = Network(n=1)
        self.assertTrue(0 in network)
        self.assertFalse(1 in network)

    def test_getitem_vertex(self):
        network = Network(n=1)
        with self.assertRaises(KeyError):
            network[1]
        self.assertIsInstance(network[0], NetworkVertex)

    def test_setitem_vertex(self):
        network = Network(n=1)
        v = network[0]
        self.assertFalse('weight' in v)
        v['weight'] = 40
        self.assertEqual(network[0]['weight'], 40)
        del v['weight']
        self.assertFalse('weight' in network[0])

    def test_delitem_vertex(self):
        network = Network(n=1)
        with self.assertRaises(NotImplementedError):
            del network[0]
            with self.assertRaises(KeyError):
                del network[0]
            self.assertEquals(len(network.vertices),
                              network.vtk.GetNumberOfVertices())

    def test_add_edge_directed(self):
        # Test setitem, contains and getitem
        network = Network(n=2)
        network.add_edge(0, 1)
        with self.assertRaises(AttributeError):
            network.add_edge(0, 1)
        self.assertTrue(network[0, 1], 1)
        self.assertTrue((0, 1) in network)
        self.assertEquals(len(network.edges), network.vtk.GetNumberOfEdges())

    def test_add_edge_undirected(self):
        # Test setitem, contains and getitem
        network = Network(n=2)
        network.add_edge(0, 1)
        with self.assertRaises(AttributeError):
            network.add_edge(0, 1)
        network[0, 1]['d'] = -1
        self.assertTrue((0, 1) in network)
        self.assertTrue((1, 0) in network)
        self.assertEquals(network[0, 1]['d'], -1)
        self.assertEquals(network[1, 0]['d'], -1)
        self.assertEquals(len(network.edges), network.vtk.GetNumberOfEdges())

    def test_add_edge_error(self):
        network = Network(n=1)
        with self.assertRaises(AttributeError):
            network.add_edge(0, 1)
        self.assertEquals(len(network.edges), network.vtk.GetNumberOfEdges())

    def test_del_edge(self):
        network = Network(n=3)
        network.add_edge(0, 1)
        network.add_edge(0, 2)
        network.add_edge(1, 2)
        del network[0, 1]
        with self.assertRaises(KeyError):
            network[0, 1]
        self.assertEquals(network[0, 2].eid, 2)
        self.assertEquals(network[1, 2].eid, 2)
        self.assertEquals(len(network.edges), network.vtk.GetNumberOfEdges())

    def test_getitem_error(self):
        network = Network()
        with self.assertRaises(NotImplementedError):
            del network[0]

    def test_setitem_error(self):
        network = Network()
        with self.assertRaises(AttributeError):
            network[0] = 1
        with self.assertRaises(AttributeError):
            network[0, 1] = 1

#    It is permitted now.
#    def test_contains_error(self):
#        network = Network()
#        with self.assertRaises(KeyError):
#            contains = '0' in network

    def test_add_edges_error(self):
        network = Network(n=5, directed=False)
        with self.assertRaises(AttributeError):
            network.add_edges([(0, 1), (1, 0)])
        self.assertEquals(len(network.edges), network.vtk.GetNumberOfEdges())

    def test_autoloop(self):
        network = Network(n=1, directed=False)
        network.add_edge(0, 0)
        self.assertTrue((0, 0) in network)

    def test_add_edges_directed(self):
        network = Network(n=5, directed=True)
        network.add_edges([(0, 1), (2, 1), (1, 0), (4, 1), (0, 0)])
        self.assertTrue(len(network), 5)
        self.assertEquals(len(network.edges), network.vtk.GetNumberOfEdges())

    def test_add_edges_undirected(self):
        network = Network(n=5, directed=False)
        network.add_edges([(0, 1), (2, 1), (3, 1), (4, 1), (0, 0)])
        self.assertTrue(len(network), 5)
        self.assertEquals(len(network.edges), network.vtk.GetNumberOfEdges())

    def test_edges_directed(self):
        network = Network(n=5, directed=True)
        network.add_edges([(0, 1), (2, 1), (0, 3), (4, 1), (0, 0), (2, 0)])
        fn = lambda edge: edge.pair
        ein = network[0].edges('in', fn)
        self.assertEquals(set(ein), set([(2, 0), (0, 0)]))
        eout = network[0].edges('out', fn)
        self.assertEquals(set(eout), set([(0, 0), (0, 1), (0, 3)]))
        eall = network[0].edges('all', fn)
        self.assertEquals(set(eall), set([(0, 0), (0, 1), (0, 3), (2, 0)]))

    def test_edges_undirected(self):
        network = Network(n=5, directed=False)
        vertices = [(0, 1), (0, 3), (0, 0), (2, 0)]
        network.add_edges(vertices + [(4, 1), (2, 1)])
        fn = lambda edge: edge.pair
        ein = network[0].edges('in', fn)
        self.assertEquals(set(vertices), set(ein))
        eout = network[0].edges('out', fn)
        self.assertEquals(set(vertices), set(eout))
        eall = network[0].edges('all', fn)
        self.assertEquals(set(vertices), set(eall))
        # Check if all edges are deleted as well
        with self.assertRaises(NotImplementedError):
            del network[0]
            self.assertListEqual(network[3].edges('all', fn), [])

    def test_neighbors_undirected(self):
        network = Network(n=5, directed=False)
        network.add_edges([(0, 1), (2, 1), (3, 1), (4, 1), (0, 0)])
        self.assertEqual(network[0].neighbors(), set([network[0], network[1]]))

    def test_iterator(self):
        network = Network(n=5)
        l = [v.vid for v in network]
        self.assertListEqual(l, range(5))

    def test_vertices_element_wise_set(self):
        network = Network(n=6)
        network.vertices['color'] = 'white'
        for v in network:
            self.assertEquals(v['color'], 'white')

    def test_vertices_element_wise_del(self):
        network = Network(n=6)
        network.vertices['color'] = 'white'
        del network.vertices['color']
        for v in network:
            self.assertFalse('color' in v)

    def test_vertices_element_wise_del_error(self):
        network = Network(n=6)
        network.vertices['color'] = 'white'
        del network[3]['color']
        self.assertFalse('color' in network[3])
        with self.assertRaises(KeyError):
            del network.vertices['color']

    def test_edges_element_wise_set(self):
        network = Network(n=6)
        network.add_edges([(0, 1), (3, 4)])
        network.edges['weight'] = 0.1
        for e in network.edges:
            self.assertEquals(network[e]['weight'], 0.1)

    def test_edges_element_wise_del(self):
        network = Network(n=6)
        network.add_edges([(0, 1), (3, 4)])
        network.edges['weight'] = 'weight'
        del network.edges['weight']
        for e in network.edges:
            self.assertTrue('weight' not in network[e])

    def test_edges_element_wise_del_error(self):
        network = Network(n=6)
        network.add_edges([(0, 1), (3, 4)])
        network.edges['weight'] = 'white'
        del network[0, 1]['weight']
        self.assertFalse('weight' in network[3])
        with self.assertRaises(KeyError):
            del network.edges['weight']


def suite():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    suite.addTests(loader.loadTestsFromTestCase(TestNetworkVertex))
    suite.addTests(loader.loadTestsFromTestCase(TestNetworkEdge))
    suite.addTests(loader.loadTestsFromTestCase(TestNetwork))
    return suite

if __name__ == '__main__':
    res = unittest.TextTestRunner(verbosity=2).run(suite())
    sys.exit(not res.wasSuccessful())
