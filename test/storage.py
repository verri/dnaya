# -*- encoding: utf-8 -*-
# Copyright © 2014, Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.
import sys
import os
import unittest

sys.path.append('.')
from dnaya.storage import ObjectStorage, NetworkStorage
from dnaya.api import Network


class TestObjectStorageRunner(unittest.TestCase):

    filename = 'persistence_test.dco'

    def setUp(self):
        self.delete_file()

    def file_exists(self):
        return os.path.isfile(self.filename)

    def delete_file(self):
        if self.file_exists():
            os.remove(self.filename)

    def test_writing_and_reading(self):
        o = ObjectStorage()
        o.header = {'name': 12387321313123}
        o.data = unittest.TestSuite()
        with open(self.filename, 'wb') as output:
            o.write(output)
        self.assertTrue(self.file_exists)

        i = ObjectStorage()
        with open(self.filename, 'rb') as input:
            i.read(input)
        self.assertEquals(o.header, i.header)
        self.assertEquals(o.data, i.data)

    def test_writing_and_reading_header(self):
        o = ObjectStorage()
        o.header = {'name': 12387321313123}
        o.data = unittest.TestSuite()
        with open(self.filename, 'wb') as output:
            o.write(output)
        self.assertTrue(self.file_exists)

        i = ObjectStorage()
        with open(self.filename, 'rb') as input:
            i.read_header(input)
        self.assertEquals(o.header, i.header)
        self.assertEquals(i.data, {})

    def tearDown(self):
        self.delete_file()


class TestNetworkStorageRunner(unittest.TestCase):

    filename = 'network_persistence_test.cno'

    def test_writing_and_reading_undirected(self):
        N = Network(n=5, directed=False)
        N['name'] = {'desc': 'my network data'}
        N[2]['weight'] = 9
        elist = [(0, 1), (3, 4), (3, 2)]
        N.add_edges(elist)
        N[3, 2]['name'] = 'Edge name'
        writer = NetworkStorage()
        writer.set_network(N)
        writer.save(self.filename)
        self.assertTrue(os.path.isfile(self.filename))

        reader = NetworkStorage()
        reader.load(self.filename)
        self.assertFalse(reader.network.directed)
        self.assertEquals(reader.network['name'], {'desc': 'my network data'})
        self.assertTrue(reader.data, writer.data)
        self.assertTrue(reader.network[2]['weight'], 9)
        for p in elist:
            self.assertTrue(p in reader.network)

    def test_writing_and_reading_directed(self):
        N = Network(n=5, directed=True)
        N['start'] = 'my network data'
        N[2]['weight'] = 9
        elist = [(0, 1), (3, 4), (3, 2)]
        N.add_edges(elist)
        N[3, 2]['name'] = 'Edge name'
        writer = NetworkStorage()
        writer.set_network(N)
        writer.save(self.filename)
        self.assertTrue(os.path.isfile(self.filename))

        reader = NetworkStorage()
        reader.load(self.filename)
        self.assertTrue(reader.network.directed)
        self.assertEquals(reader.network['start'], 'my network data')
        self.assertTrue(reader.data, writer.data)
        self.assertTrue(reader.network[2]['weight'], 9)
        for p in elist:
            self.assertTrue(p in reader.network)

    def tearDown(self):
        if os.path.isfile(self.filename):
            os.remove(self.filename)


def suite():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    suite.addTests(loader.loadTestsFromTestCase(TestObjectStorageRunner))
    suite.addTests(loader.loadTestsFromTestCase(TestNetworkStorageRunner))
    return suite

if __name__ == '__main__':
    res = unittest.TextTestRunner(verbosity=2).run(suite())
    sys.exit(not res.wasSuccessful())
