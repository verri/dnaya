# -*- encoding: utf-8 -*-
# Copyright © 2014, Filipe Verri.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

import sys
import unittest

from PyQt4.QtCore import QVariant

sys.path.append('.')
from dnaya.config import Settings


class TestSettingsRunner(unittest.TestCase):

    cases = {'integer': int(10),
             'float': float(0.1),
             'string': 'foobar',
             'list same type': [1, 2, 3],
             'list multi-type': ['foo', 1],
             'dict': {'one': 1, 'two': 'two'}}

    def test_writing_and_reading(self):
        settings = Settings()
        for key, value in self.cases.iteritems():
            settings[key] = value
        del settings

        settings = Settings()
        for key, value in self.cases.iteritems():
            self.assertIsNot(settings[key], QVariant)
            self.assertEquals(settings[key], value)

        with self.assertRaises(KeyError):
            settings['bla']


def suite():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    suite.addTests(loader.loadTestsFromTestCase(TestSettingsRunner))
    return suite

if __name__ == '__main__':
    res = unittest.TextTestRunner(verbosity=2).run(suite())
    sys.exit(not res.wasSuccessful())
