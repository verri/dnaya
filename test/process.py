# -*- encoding: utf-8 -*-
# Copyright © 2014, Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.
import sys
import unittest
import signal

from PyQt4 import QtGui

sys.path.append('.')
from dnaya.api import DynamicalProcess, Network
from dnaya.task import TaskContext

signal.signal(signal.SIGINT, signal.SIG_DFL)


class MyProcess(DynamicalProcess):
    author = 'Urio'

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)

    def request_form(self):
        return {}

    def on_load(self, form):
        self.network = Network(10)

    def on_update_epoch(self, epoch_id):
        if epoch_id == 10:
            raise StopIteration

    def on_update_vertex(self, vid):
        pass


def my_task(network):
    return 'This is my_task result'


class Widget(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self)

    def setup(self, process, app):
        self.process = process
        self.app = app
        process.epoch_processed.connect(self.epoch_callback)
        process.vertex_processed.connect(self.vertex_callback)
        process.finished.connect(self.exit)
        process.epochs_per_sec = 3

    def epoch_callback(self, epoch):
        self.task = TaskContext(my_task)
        self.task.done.connect(self.my_task_is_done)
        self.process.add_task(self.task)

    def vertex_callback(self, vid):
        pass

    def my_task_is_done(self):
        print 'Task result:', self.task.result

    def exit(self):
        self.app.exit()


class TestProcess(unittest.TestCase):

    def test_widget(self):
        app = QtGui.QApplication(sys.argv)

        process = MyProcess(None)
        process.on_load({})

        w = Widget()
        w.setup(process, app)

        process.start()
        app.exec_()


def suite():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    suite.addTests(loader.loadTestsFromTestCase(TestProcess))
    return suite

if __name__ == '__main__':
    res = unittest.TextTestRunner(verbosity=2).run(suite())
    sys.exit(not res.wasSuccessful())
