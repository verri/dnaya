#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

import signal
import sys

from PyQt4.QtGui import QApplication

from dnaya.ui.mainwindow import MainWindow

signal.signal(signal.SIGINT, signal.SIG_DFL)

app = QApplication(sys.argv)
app.setApplicationName('DNAYA')

window = MainWindow(app)
window.show()

app.exec_()
