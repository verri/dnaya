

.PHONY: all test

all:

test:
	python2 ./test/network.py
	python2 ./test/storage.py
	python2 ./test/process.py
	python2 ./test/storage.py
	python2 ./test/settings.py

pep8:
	autopep8 -r -i ./

clean:
	find . -type f -name '*.pyc' -exec rm {} \;
	find . -depth -type d -name '__pycache__' -exec rm -r {} \;

