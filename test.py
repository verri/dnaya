from PyQt4.QtGui import QApplication, QDialog, QVBoxLayout, QPushButton

from vtk import vtkMutableDirectedGraph

from dnaya.ui.mainwindow import MainWindow

# XXX
from dnaya.api import DynamicalProcess, Network
from dnaya.ui.visualization import NetworkVisualization
from random import uniform


class TestProcess(DynamicalProcess):
    author = 'Verri'

    def __init__(self):
        self.i = 0
        self.network = Network(10)

    def request_form(self):
        return {}

    def on_load(self, form):
        pass

    def on_update_epoch(self):
        print 'epoch: ', self.i
        if self.i == 10:
            raise StopIteration
        self.i = self.i + 1

    def on_update_vertex(self, vid):
        print 'vertex: ', vid

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    network = vtkMutableDirectedGraph()
    for i in range(10):
        network.AddVertex()
    for i in range(20):
        network.AddGraphEdge(int(uniform(0, 10)),
                             int(uniform(0, 10)))

    def add_vertex():
        network.AddVertex()
        vis.refresh()

    def add_edge():
        network.AddGraphEdge(int(uniform(0, network.GetNumberOfVertices())),
                             int(uniform(0, network.GetNumberOfVertices())))
        vis.refresh()

    vis = NetworkVisualization(network)
    button = QPushButton('Add Vertex')
    button1 = QPushButton('Add Edge')
    button2 = QPushButton('Refresh')
    button.clicked.connect(add_vertex)
    button1.clicked.connect(add_edge)
    button2.clicked.connect(lambda: vis.refresh())
    layout = QVBoxLayout()
    layout.addWidget(vis)
    layout.addWidget(button)
    layout.addWidget(button1)
    layout.addWidget(button2)
    dialog = QDialog(window)
    dialog.setLayout(layout)
    dialog.show()

    app.exec_()
