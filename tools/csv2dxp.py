import string
from random import choice
from numpy import genfromtxt, array, empty, zeros, unique, apply_along_axis,\
    fromstring, sqrt
from scipy.spatial.distance import pdist
from zipfile import ZipFile
from cPickle import dump


def string_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(choice(chars) for x in range(size))


class DXPFile(ZipFile):

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

    def get_D(self):
        return fromstring(self.read('D.bin'))

    def get_X(self):
        return fromstring(self.read('X.bin'))

    def get_p(self):
        return fromstring(self.read('p.bin'))

    @classmethod
    def Create(cls, filename, D, X, p):
        f = DXPFile(filename, 'w')

        dstr = D.tostring()
        xstr = X.tostring()
        pstr = p.tostring()

        f.writestr('D.bin', dstr)
        f.writestr('X.bin', xstr)
        f.writestr('p.bin', pstr)

        return f


def normalize(M, axis=0):
    return apply_along_axis(lambda x: (x - x.min()) / (x.max() - x.min()), axis, M)


def csv2dxp(ifile, ofile):
    dataset = genfromtxt(ifile, dtype=None, delimiter=',', names=None)

    N = len(dataset)
    if N == 0:
        return None
    d = len(dataset[0])

    X = empty([N, d])
    for i in range(0, N):
        X[i, :] = dataset[i]
    X = normalize(X)

    D = pdist(X) / sqrt(d)

    y = list()
    for i in range(0, N):
        y.append(dataset[i][-1])
    classes = unique(y)

    if len(classes) < 2:
        return None

    print 'Classes:'
    for i in range(0, len(classes)):
        print '(' + str(i) + ')', classes[i]
    c = classes[int(raw_input('Select the positive class: '))]

    p = zeros(N)
    for i in range(0, N):
        if y[i] == c:
            p[i] = 1

    return DXPFile.Create(ofile, D, X, p)

if __name__ == '__main__':
    import sys

    if len(sys.argv) < 3:
        print 'usage: ./csv2dxp.py INPUT_FILE OUTPUT_FILE'

    ifile = sys.argv[1]
    ofile = sys.argv[2]

    dxpfile = csv2dxp(ifile, ofile)
    if dxpfile is None:
        sys.exit(1)
    dxpfile.close()
