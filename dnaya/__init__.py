# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

__author__ = 'Filipe Verri and Paulo Urio'
__copyright__ = 'Copyright © 2014, Filipe Verri'
__license__ = 'zlib/libpng'
__version__ = '0'
