# -*- coding: utf-8 -*-
# Copyright © 2014, Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

import os
import pickle
from struct import pack, unpack
from time import time
import zlib

from dnaya.config import Settings
from dnaya.network import Network

SIGNATURE = '\x89' + 'DNAYA' + '1a'


def generate_filename(basedir, extension=''):
    if not basedir.endswith(os.sep):
        basedir += os.sep
    generate = lambda: '{d}{f:f}{e}'.format(d=basedir, f=time(), e=extension)
    fn = generate()
    while os.path.exists(fn):
        fn = generate()
    return fn


class FileFormatError(Exception):

    def __init__(self, value):
        r = repr(value)
        e = repr(SIGNATURE)
        self.message = 'Expected {} and read {}'.format(e, r)


class NetworkReaderError(Exception):

    def __init__(self, expected, read):
        m = 'expected vid={}, read vid={}'.format(expected, read)
        self.message = 'Could not load the network from file - ' +\
            'inconsistent indexes: ' + m


class ObjectStorage(object):

    """Writes and reads an object into files. Each file contains a header
    and the object data."""

    def __init__(self):
        self.header = {}
        self.data = {}

    def __str__(self):
        return '%s (header=%r, object=%r)' % (self.__class__, self.header,
                                              self.data)

    def _read_signature(self, file):
        file.seek(0)
        file_signature = file.read(len(SIGNATURE))
        if file_signature != SIGNATURE:
            raise FileFormatError(file_signature)

    def encode(self, data):
        '''Compresses an arbitrary |data| object'''
        return zlib.compress(pickle.dumps(data))

    def decode(self, data):
        '''Loads a compressed |data| object'''
        return pickle.loads(zlib.decompress(data))

    def read(self, file):
        """Read object from |file|."""
        self.read_header(file)
        self.read_object(file)

    def read_header(self, file):
        """Read only the |file| header.  This is useful when the object
        is large and we don't want to decode it."""
        self._read_signature(file)
        obj_header_size = unpack('i', file.read(4))[0]
        self.header = self.decode(file.read(obj_header_size))
        return self.header

    def read_object(self, file):
        """Ready the |file| object.  It assumes |file| is already in the
        object beginning position."""
        self.data = self.decode(file.read())

    def write(self, file):
        """Write to |file|.
        File format: {signature}{header size}{header}{object data}"""
        header = self.encode(self.header)
        data = self.encode(self.data)
        file.write(SIGNATURE)
        file.write(pack('i', len(header)))
        file.write(header)
        file.write(data)

    @property
    def header(self):
        return self._header

    @header.setter
    def header(self, value):
        self._header = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value


class NetworkStorage(ObjectStorage):

    def __init__(self):
        ObjectStorage.__init__(self)
        self.network = None
        self.last_filename = None  # Last opened header

    def _build_header(self):
        self.header.update({'directed': self.network.directed,
                            'vcount': len(self.network.vertices),
                            'ecount': len(self.network.edges),
                            'timestamp': time()})

    def _build_data(self):
        if not isinstance(self.network, Network):
            raise ValueError('Set a network before saving.')
        self.data = {'directed': self.network.directed,
                     'data': self.network.data}
        self._build_vertices_data()
        self._build_edges_data()

    def _build_dict(self, dictionary):
        """Build the |dictionary| of vertices and edges data, instead
        storing the instances directly."""
        data = dict()
        for key, value in dictionary.iteritems():
            data[key] = value.data
        return data

    def _build_vertices_data(self):
        self.data['v_data'] = self._build_dict(self.network.vertices)

    def _build_edges_data(self):
        self.data['e_data'] = self._build_dict(self.network.edges)

    def _ensure_path(self, fname):
        dir = os.path.dirname(fname)
        if dir and not os.path.exists(dir):
            os.makedirs(dir)

    def _read_network_from_data(self):
        """
        One assumption was made while writing this code.
         (1) The vertices are in order [0, 1, ..] without index holes.
             Any hole in this index would break the consistency between
             our own representation and the vtk's.
        """
        N = Network(directed=self.data['directed'])
        N._data = self.data['data']
        for key, value in self.data['v_data'].iteritems():
            v = N.add_vertex()
            if key != v.vid:
                raise NetworkReaderError(expected=v.vid, read=key)
            v.data = value
        for key, value in self.data['e_data'].iteritems():
            e = N.add_edge(*key)
            e.data = value
        self.set_network(N)

    def load(self, filename, header_only=False):
        """Load the network stored in |filename|.  If the network is large,
        you may set |header_only| to avoid decoding the network data."""
        with open(filename, 'rb') as input:
            self.read_header(input)
            if not header_only:
                self.read_object(input)
                self._read_network_from_data()
        self.last_filename = filename

    def save(self, filename=''):
        """Write |self.network| to |filename|.  If filename is empty, a filename
        will be generated."""
        self._build_header()
        self._build_data()
        if not filename:
            settings = Settings()
            filename = generate_filename(
                settings.get('network_directories')[0])
        self._ensure_path(filename)
        with open(filename, 'wb') as output:
            self.write(output)

    def sync(self):
        """Reload the last loaded network.  It's useful when only the header 
        has been read."""
        if not self.last_filename:
            raise Exception('No network has been loaded.')
        self.load(self.last_filename)

    def set_network(self, value):
        self.network = value
