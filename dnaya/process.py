# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

import itertools
from time import time, sleep
from Queue import Queue

from PyQt4.QtCore import pyqtSignal, QMutex, QString, QThread, QWaitCondition

from dnaya.storage import NetworkStorage


def compute_delay(m, start, end):
    """Find out how many milliseconds are still needed to get 1/m second."""
    delay = 1.0 / m - (end - start)
    return max(delay, 0.0)


class DynamicalProcess(QThread):

    """Base class for a dynamical process plugin."""

    epoch_processed = pyqtSignal(int)
    vertex_processed = pyqtSignal(int)
    finished = pyqtSignal()
    paused = pyqtSignal()
    resumed = pyqtSignal()
    progress_changed = pyqtSignal(int)
    status_changed = pyqtSignal(QString)
    title_changed = pyqtSignal(QString)

    author = ''
    description = ''
    menu = ''

    def __init__(self, parent=None):
        QThread.__init__(self, parent)

        self._instance_name = repr(self)
        self.vertices_per_sec = 0.0
        self.epochs_per_sec = 0.0
        self._finished = False
        self._count = itertools.count()
        self._title = QString()

        self._paused = False
        self._sync = QMutex()
        self._pause_condition = QWaitCondition()

        self._tasks = Queue()

    @classmethod
    def get_menu(self):
        return self.menu if self.menu else repr(self)

    @classmethod
    def get_description(self):
        return self.description if self.description else repr(self)

    def _trigger_on_update_vertex(self, vertex):
        '''Calls plugin's |on_update_vertex|'''
        start = time()
        if not self._tasks.empty():
            self._run_tasks()
        self.on_update_vertex(vertex.vid)
        end = time()
        return compute_delay(self.vertices_per_sec, start, end)

    def _traverse(self):
        '''Process |vertex| reserving up to 1/m second for each one.'''
        for vertex in self.network:
            self._check_paused()
            delay = self._trigger_on_update_vertex(vertex)
            self.vertex_processed.emit(vertex.vid)
            sleep(delay)

    def _epoch(self, epoch_id):
        '''Process one epoch reserving up to 1/m second.'''
        self._check_paused()
        start = time()
        try:
            self.on_update_epoch(epoch_id)
            self._traverse()
        except StopIteration:
            self._finished = True
            self.set_progress(100)
            self.set_status('Finished.')
        end = time()
        self.epoch_processed.emit(epoch_id)
        return compute_delay(self.epochs_per_sec, start, end)

    def _run(self):
        delay = self._epoch(self._count.next())
        sleep(delay)

    def _run_tasks(self):
        while not self._tasks.empty():
            task = self._tasks.get()
            task.run()
            task.done.emit()
            self._tasks.task_done()

    def run(self):
        self.resumed.emit()
        while not self.has_finished:
            self._run()
        self.save_network()
        self.finished.emit()

    def _check_paused(self):
        self._sync.lock()
        if (self._paused):
            self._pause_condition.wait(self._sync)
        self._sync.unlock()

    def pause(self):
        if self.isRunning():
            self._sync.lock()
            self._paused = True
            self._sync.unlock()
            self.paused.emit()

    def resume(self):
        if self.isRunning() and self._paused:
            self._sync.lock()
            self._paused = False
            self._sync.unlock()
            self._pause_condition.wakeAll()
            self.resumed.emit()

    def save_network(self):
        self.set_status('Saving...')
        nv = NetworkStorage()
        nv.set_network(self.network)
        nv.header['name'] = self.title.toUtf8()
        nv.header['description'] = ''
        nv.save()

    def add_task(self, task_context):
        '''Add |task| to the process pool.  Note it must be read-only.'''
        task_context.set_network(self.network)
        self._tasks.put_nowait(task_context)

    def assign_form(self, form):
        '''This method can be used to assign every form entry as an instance
        attribute. Instead acessing form['N'], it will be accessible as
        self.N.  Take note that some entries that aren't a valid syntax,
        such as form['! a'] will only be accessible via getattr(self, '! a').'''
        for k, v in form.iteritems():
            setattr(self, k, v)

    def set_progress(self, value):
        """
        Set the progress bar with the integer |value| in [0, 100]
        """
        self.progress_changed.emit(value)

    def set_status(self, message):
        """
        Set the one-line status text with |message|
        """
        self.status_changed.emit(message)

    def request_form(self):
        """
        Returns a dict with the parameters specification the user needs to fill
        out.
        Syntax: {'parameter name': { 'property': value, ... } }
            Example:
                return {'N': {'type': 'integer', 'min': 1}}
                It tells it wants a positive integer parameter named N.
                At |on_load|, its value will be available at form['N'].

        Special parameter names
        !validate - (optional) Set an user-defined validator method, which may
                    block an user to create the process on invalid input data.
                    In the validator, throw ValueError exception to invalidate.
            Example:
                def check(self, form):
                    if form['N'] % 2:
                        raise ValueError('N must be an even number.')
                def request_form(self):
                    return {'N': {'type': 'integer'}, '!validate': self.check}

        Data types and their properties
                    min max step    precision   default tip
        integer     x   x   x                   x       x
        real        x   x   x       x           x       x
        boolean                                 x       x
        string                                  x       x
        file
        network
        """
        return {}

    def on_load(self, form):
        """
        Initialization method given a |form| input from the user.
        """
        pass

    def on_update_epoch(self, epoch_id):
        """
        When a network traverse will be performed, an epoch update is called.
        This method asks the plugin whether it should continue or not.
        """
        raise StopIteration

    def on_update_vertex(self, vertex):
        """
        While traversing, this method is called for each |vertex| instance.
        """
        pass

    @property
    def epochs_per_sec(self):
        return self._epochs_per_sec

    @epochs_per_sec.setter
    def epochs_per_sec(self, value):
        if value > 0.0:
            self._epochs_per_sec = value
        else:
            self._epochs_per_sec = float('inf')

    @property
    def has_finished(self):
        return self._finished

    @property
    def title(self):
        return self._title

    def set_title(self, v):
        if not v:
            title = repr(self)
        else:
            title = v
        if not isinstance(title, QString):
            title = QString.fromUtf8(title)
        self._title = title
        self.title_changed.emit(self._title)

    @property
    def network(self):
        return self._network

    @network.setter
    def network(self, value):
        self._network = value

    @property
    def vertices_per_sec(self):
        return self._vertices_per_sec

    @vertices_per_sec.setter
    def vertices_per_sec(self, value):
        if value > 0:
            self._vertices_per_sec = value
        else:
            self._vertices_per_sec = float('inf')
