# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from PyQt4.QtCore import pyqtSignal, pyqtSlot, QDateTime, QPoint, QString, Qt
from PyQt4.QtGui import QHBoxLayout, QMenu, QTreeWidget, QTreeWidgetItem,\
    QWidget

from dnaya.loader import NetworkLoader


class NetworkTree(QTreeWidget):

    valid_item_selected = pyqtSignal(bool)

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)

        self._create_ui_elements()
        self._setup_ui()

    def _create_ui_elements(self):
        self.setHeaderLabels(['Name', 'Date', 'Summary'])
        self._menu = QMenu(self)
        self._action_view = self._menu.addAction('&View')
        self.sync()

    def _setup_ui(self):
        self.setSortingEnabled(True)
        self.sortByColumn(1, Qt.AscendingOrder)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.context_menu_requested)
        self.itemSelectionChanged.connect(self._item_selected)

    def _get_date(self, reader):
        tstamp = int(reader.header.get('timestamp', 0))
        date = QDateTime()
        date.setTime_t(tstamp)
        return date

    def _add_network_item(self, reader):
        name = reader.header.get('name', '<Unknown>')
        date = self._get_date(reader)
        summary = self._build_summary(reader)

        splitted = name.split('/')

        parent_item = None
        while len(splitted) > 1:
            items = self.findItems(splitted[0], Qt.MatchFixedString)
            if items:
                parent_item = items[0]
            else:
                parent_item = QTreeWidgetItem(parent_item)
                parent_item.setText(0, QString.fromUtf8(splitted[0]))
                self.addTopLevelItem(parent_item)
            del splitted[0]

        item = QTreeWidgetItem(parent_item)
        item.setText(0, QString.fromUtf8(splitted[0]))
        item.setData(1, Qt.DisplayRole, date)
        item.setText(2, QString.fromUtf8(summary))
        item.setData(0, Qt.UserRole, reader)
        if parent_item is None:
            self.addTopLevelItem(item)

    def _build_summary(self, reader):
        d = reader.header.get('directed', '?      ')
        if isinstance(d, bool):
            d = 'Directed' if d else 'Undirected'
        v = reader.header.get('vcount', '?')
        e = reader.header.get('ecount', '?')
        return '{}\tV={}\tE={}'.format(d, v, e)

    def _item_selected(self):
        item = self.selected_item()
        valid = False
        if item:
            obj = item.data(0, Qt.UserRole).toPyObject()
            valid = obj is not None
        self.valid_item_selected.emit(valid)

    @pyqtSlot(QPoint)
    def context_menu_requested(self, position):
        pass
        # self._menu.exec_(self.mapToGlobal(position))

    def selected_item(self):
        item = None
        sel = self.selectedItems()
        if sel:
            item = sel[0]
        return item

    def sync(self):
        self.clear()
        for reader in NetworkLoader():
            self._add_network_item(reader)
        self.resize_columns()

    def resize_columns(self):
        self.expandAll()
        for i in xrange(self.columnCount()):
            self.resizeColumnToContents(i)

    @property
    def selected_network_storage(self):
        item = self.selected_item()
        reader = item.data(0, Qt.UserRole).toPyObject()
        return reader


class NetworkManager(QWidget):

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)

        self._create_ui_elements()
        self._setup_ui()

    def _create_ui_elements(self):
        self._tree = NetworkTree(self)
        self._tree.valid_item_selected.connect(self.item_selected)

    def _setup_ui(self):
        layout = QHBoxLayout()
        layout.addWidget(self._tree)
        self.setLayout(layout)

    @pyqtSlot(bool)
    def item_selected(self, valid):
        if valid:
            print self._tree.selected_network_storage

    def sync(self):
        self._tree.sync()
