# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from PyQt4.QtCore import pyqtSlot, QString, Qt
from PyQt4.QtGui import QAbstractSpinBox, QCheckBox, QComboBox, QDialog, \
    QDialogButtonBox, QDoubleSpinBox, QFormLayout, QFileDialog, QGroupBox, \
    QHBoxLayout, QLabel, QLineEdit, QMessageBox, QPushButton, QSpinBox,\
    QVBoxLayout, QWidget

from dnaya.ui.visualization import VisualizationForm
from dnaya.ui.network import NetworkTree


class NetworkChooserDialog(QDialog):

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)

        self.setWindowTitle('Choose a network')
        self.setModal(True)

        self._create_ui_elements()
        self._setup_ui()
        self._setup_slots()

    def _create_ui_elements(self):
        self.networks = NetworkTree()
        self.buttons = QDialogButtonBox(QDialogButtonBox.Ok |
                                        QDialogButtonBox.Cancel,
                                        Qt.Horizontal, self)
        button_ok = self.buttons.button(QDialogButtonBox.Ok)
        button_ok.setEnabled(False)

    def _setup_ui(self):
        layout = QVBoxLayout()
        layout.addWidget(self.networks)
        layout.addWidget(self.buttons)
        self.setLayout(layout)
        self.resize(self.networks.size())

    def _setup_slots(self):
        button_ok = self.buttons.button(QDialogButtonBox.Ok)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)
        self.networks.valid_item_selected.connect(button_ok.setEnabled)
        self.networks.doubleClicked.connect(button_ok.click)

    def exec_(self):
        if QDialog.exec_(self) == QDialog.Accepted:
            return self.networks.selected_network_storage


class NetworkChooserWidget(QWidget):

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)
        self.storage = None

        self.label = QLabel()
        self.button = QPushButton('...')

        layout = QHBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.button)
        self.setLayout(layout)

        self.button.clicked.connect(self.clicked)

    @pyqtSlot()
    def clicked(self):
        dialog = NetworkChooserDialog(self)
        storage = dialog.exec_()

        if storage is not None:
            self.label.setText(QString.fromUtf8(
                QString(storage.header['name'])))
            self.storage = storage

    def network(self):
        if self.storage is None:
            return
        if self.storage.network is None:
            self.storage.sync()
        return self.storage.network


class OpenFileWidget(QWidget):

    def __init__(self, data, parent=None):
        super(self.__class__, self).__init__(parent)
        self._data = data

        layout = QHBoxLayout()
        self._label = QLineEdit()
        button = QPushButton('...')
        button.clicked.connect(self.clicked)

        layout.addWidget(self._label)
        layout.addWidget(button)
        self.setLayout(layout)

    @pyqtSlot()
    def clicked(self):
        caption = self._data.get('caption', '')
        directory = self._data.get('directory', '')
        filt = self._data.get('filter', '')
        filename = QFileDialog.getOpenFileName(self, caption, directory, filt)
        self._label.setText(filename)
        self._filename = filename

    def filename(self):
        return self._filename


def _set_property(widget_function, data, key, default=None):
    """Try to assign data[key] to a given widget.  Ignore when |key| isn't
    set and |default| is null."""
    value = data.get(key, default)
    if value:
        widget_function(value)


def _set_common_properties(widget, data):
    _set_property(widget.setToolTip, data, 'tip')


def _widget_from_data(data):
    if not isinstance(data, dict):
        return

    dtype = data['type']

    if dtype == 'network':
        widget = NetworkChooserWidget()
        return widget

    # Integer -> QSpinBox
    elif dtype == 'integer':
        widget = QSpinBox()
        widget.setRange(data.get('min', -100000),
                        data.get('max', +100000))
        _set_property(widget.setSingleStep, data, 'step')
        _set_property(widget.setValue, data, 'default')
        _set_common_properties(widget, data)
        return widget

    # Real -> QDoubleSpinBox
    elif dtype == 'real':
        widget = QDoubleSpinBox()
        widget.setRange(data.get('min', -1.0),
                        data.get('max', +1.0))
        _set_property(widget.setDecimals, data, 'precision')
        _set_property(widget.setValue, data, 'default')
        _set_property(widget.setSingleStep, data, 'step')
        _set_common_properties(widget, data)
        return widget

    # Boolean -> QCheckBox
    elif dtype == 'boolean':
        widget = QCheckBox()
        _set_common_properties(widget, data)
        _set_property(widget.setValue, data, 'default')
        return widget

    # String -> QLineEdit
    elif dtype == 'string':
        widget = QLineEdit()
        _set_common_properties(widget, data)
        _set_property(widget.setText, data, 'default')
        return widget

    # File -> OpenFileWidget
    elif dtype == 'file':
        return OpenFileWidget(data)

    raise NotImplementedError


class ParametersForm(QFormLayout):

    def __init__(self, form, parent=None):
        super(self.__class__, self).__init__(parent)
        self.form = form
        self._setup_parameters(form)
        self.setVerticalSpacing(10)
        self.setHorizontalSpacing(5)

    def __str__(self):
        return 'Parameters'

    def _setup_parameters(self, form):
        inputs = {}
        names = sorted(form.keys())
        for name in names:
            data = form[name]
            widget = _widget_from_data(data)
            if widget:
                inputs[name] = widget
                self.addRow(QLabel(QString.fromUtf8(name)), widget)
        self.inputs = inputs

    def _build_result(self):
        values = {}
        for (name, widget) in self.inputs.iteritems():
            if isinstance(widget, QComboBox):
                values[name] = widget.itemData(widget.currentIndex()).copy()
            elif isinstance(widget, QCheckBox):
                values[name] = widget.checkState() == Qt.Checked
            elif isinstance(widget, QLineEdit):
                values[name] = widget.text()
            elif isinstance(widget, QAbstractSpinBox):
                values[name] = widget.value()
            elif isinstance(widget, OpenFileWidget):
                values[name] = widget.filename()
            elif isinstance(widget, NetworkChooserWidget):
                values[name] = widget.network()
            else:
                raise NotImplementedError
        self.result = values

    def accept(self):
        try:
            self._build_result()
            validator = self.form.get('!validate', lambda form: None)
            validator(self.result)
            return True
        except ValueError as e:
            s = "<b>The following conditions couldn't be satisfied:</b>\n\n"
            m = QString(s)
            m += Qt.escape(QString.fromUtf8(e.message))
            m.replace('\n', '<br />')
            QMessageBox.information(self.parent(), 'Invalid parameters', m)
        return False


class ProcessSetupDialog(QDialog):

    def __init__(self, process, parent=None):
        super(self.__class__, self).__init__(parent)
        self.process = process
        self.form = process.request_form()
        self.step = self._step1

        self.setModal(True)
        self.setWindowTitle('Setup')

        self._create_ui_elements()
        self._setup_ui()
        self._setup_slots()

    def _create_ui_elements(self):
        self._create_buttons()

    def _create_buttons(self):
        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal, self.parent())
        self.buttons = buttons
        self._set_button_text('Next')

    def _set_button_text(self, text):
        self.buttons.button(QDialogButtonBox.Ok).setText(text)

    def _setup_ui(self):
        self.vbox = QVBoxLayout(self)
        self.gbox = None
        self.vbox.addWidget(self.buttons)
        self.parameters = self._set_group(ParametersForm, self.form)

    def _setup_slots(self):
        self.buttons.accepted.connect(self.next_and_accept)
        self.buttons.rejected.connect(self.reject)

    def _set_group(self, form_layout, arg):
        """Create a |form_layout| which uses |arg| information to create
        the UI.  If a group is already showed, delete and replace it."""
        if self.gbox:
            self.gbox.deleteLater()
        self.gbox = QGroupBox(self)
        layout = form_layout(arg, self.gbox)
        self.gbox.setTitle(QString.fromUtf8(str(layout)))
        self.gbox.setLayout(layout)
        self.vbox.insertWidget(0, self.gbox)
        return layout

    def _step1(self):
        """In Parameters group.  Move to Visualizations"""
        if not self.parameters.accept():
            return
        self.process.on_load(self.parameters.result)
        self.views = self._set_group(VisualizationForm, self.process)
        self._set_button_text('Create')
        self.step = self._step2

    def _step2(self):
        """In Visualizations group.  Create visualizations and close."""
        if self.views.accept():
            self.accept()

    @pyqtSlot()
    def next_and_accept(self):
        self.step()

    def exec_(self):
        return super(self.__class__, self).exec_() == QDialog.Accepted
