# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from PyQt4.QtCore import pyqtSlot, Qt
from PyQt4.QtGui import QAction, qApp, QIcon, QMainWindow, QTabWidget

from process import ProcessManager
from network import NetworkManager


class MainWindow(QMainWindow):

    def __init__(self, app):
        super(self.__class__, self).__init__()
        self._app = app

        self.setWindowTitle('DNAYA')
        self.setWindowIcon(QIcon('img/icon.svg'))
        self.resize(800, 600)

        self._create_ui_elements()
        self._setup_menu()
        self._setup_central()
        self._setup_slots()

    def _create_ui_elements(self):
        # Menu bar
        self._action_quit = QAction('&Quit', self)
        self._action_net_refresh = QAction('&Refresh', self)

        # Central Widget
        self._workspace = QTabWidget()
        self._process_manager = ProcessManager(self)
        self._network_manager = NetworkManager(self)

    def _setup_menu(self):
        self._action_quit.setShortcut('Ctrl+Q')
        self._action_net_refresh.setShortcut(Qt.Key_F5)

        self._menu_file = self.menuBar().addMenu('&File')
        self._menu_file.addAction(self._action_quit)

        self._menu_nets = self.menuBar().addMenu('&Networks')
        self._menu_nets.addAction(self._action_net_refresh)

    def _setup_central(self):
        self._workspace.addTab(self._process_manager, 'Dynamical Processes')
        self._workspace.addTab(self._network_manager, 'Networks')
        self.setCentralWidget(self._workspace)

    def _setup_slots(self):
        self._action_quit.triggered.connect(qApp.quit)
        self._action_net_refresh.triggered.connect(self.refresh)
        self._process_manager.process_finished.connect(self.refresh)

    @pyqtSlot()
    def refresh(self):
        self._network_manager.sync()
