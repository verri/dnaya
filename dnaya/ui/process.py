# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from PyQt4.QtCore import pyqtSignal, pyqtSlot, QString, Qt
from PyQt4.QtGui import QDialog, QDialogButtonBox, QFrame, QGroupBox,\
    QHBoxLayout, QLabel, QLineEdit, QProgressBar, QPushButton, QScrollArea,\
    QSizePolicy, QSpinBox, QTreeWidget, QTreeWidgetItem, QVBoxLayout, QWidget

from dnaya.loader import PluginLoader
from dnaya.ui.setup import ProcessSetupDialog
from dnaya.ui.visualization import VisualizationManager


class UserHasAborted(Exception):

    def __init__(self):
        pass


class ProcessTree(QTreeWidget):

    valid_item_selected = pyqtSignal(bool)

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)

        self.setHeaderLabels(['Name', 'Author', 'Description'])

        for plugin_class in PluginLoader():
            self._load_plugin(plugin_class)

        self.itemSelectionChanged.connect(self.check_valid_selection)
        self._selected_plugin = None

        self.expandAll()
        for i in xrange(self.columnCount()):
            self.resizeColumnToContents(i)

    def _load_plugin(self, cls):
        name = cls.get_menu()
        splitted = name.split('/')
        parent_item = None
        while len(splitted) > 1:
            items = self.findItems(splitted[0], Qt.MatchFixedString)
            if items:
                parent_item = items[0]
            else:
                parent_item = QTreeWidgetItem(parent_item)
                parent_item.setText(0, QString.fromUtf8(splitted[0]))
                self.addTopLevelItem(parent_item)
            del splitted[0]
        item = QTreeWidgetItem(parent_item)
        item.setText(0, QString.fromUtf8(splitted[0]))
        item.setText(1, QString.fromUtf8(cls.author))
        item.setText(2, QString.fromUtf8(cls.get_description()))
        item.setData(0, Qt.UserRole, cls)
        if parent_item is None:
            self.addTopLevelItem(item)

    @pyqtSlot()
    def check_valid_selection(self):
        item = self.selectedItems()[0]
        data = item.data(0, Qt.UserRole)

        if data.isValid():
            self._selected_plugin = data.toPyObject()
            self.valid_item_selected.emit(True)
        else:
            self._selected_plugin = None
            self.valid_item_selected.emit(False)

    @property
    def selected_plugin(self):
        return self._selected_plugin


class ProcessChooserDialog(QDialog):

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)

        self.setWindowTitle('Choose a dynamical process')
        self.processes = ProcessTree()
        self._add_buttons()
        self._add_layout()

    def _add_buttons(self):
        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        self.buttons = buttons

        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)

        button_ok = buttons.button(QDialogButtonBox.Ok)
        button_ok.setEnabled(False)
        self.processes.valid_item_selected.connect(button_ok.setEnabled)

        self.processes.doubleClicked.connect(button_ok.click)

    def _add_layout(self):
        layout = QVBoxLayout()
        layout.addWidget(self.processes)
        layout.addWidget(self.buttons)
        self.setLayout(layout)
        self.resize(self.processes.size())

    def exec_(self):
        return QDialog.exec_(self) == QDialog.Accepted

    @property
    def result(self):
        return self.processes.selected_plugin


class ProcessChooser(QWidget):

    process_created = pyqtSignal(type)

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)
        self.process = None
        self._create_ui_elements()
        self._setup_layout()
        self._setup_signals()

    def _create_ui_elements(self):
        self._label = QLabel('Please select a dynamical process.')
        self._label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self._label.setFrameStyle(QFrame.Sunken + QFrame.Panel)

        self._button_choose = QPushButton('Choose...')
        self._button_create = QPushButton('Create')
        self._button_create.setEnabled(False)

    def _setup_layout(self):
        layout = QHBoxLayout()
        layout.setMargin(0)
        layout.addWidget(self._label)
        layout.addWidget(self._button_choose)
        layout.addWidget(self._button_create)
        self.setLayout(layout)

    def _setup_signals(self):
        self._button_choose.clicked.connect(self.choose_process)
        self._button_create.clicked.connect(self.create_process)

    def _update_selected(self):
        self._label.setText(QString.fromUtf8(self.process.get_description()))
        self._button_create.setEnabled(True)

    @pyqtSlot()
    def choose_process(self):
        dialog = ProcessChooserDialog(self)
        if dialog.exec_():
            self.process = dialog.result
            self._update_selected()
            self.create_process()
        dialog.setParent(None)

    @pyqtSlot()
    def create_process(self):
        self.process_created.emit(self.process)


class QInPlaceLineEdit(QLineEdit):

    """
    A QLineEdit that mocks itself as an editable QLabel.  The user may change
    the text and confirm it with a return or escape to cancel it.
    """

    def __init__(self, contents='', parent=None):
        super(self.__class__, self).__init__(contents, parent)
        self.setStyleSheet('QLineEdit { background-color : rgba(0,0,0,0%); }')
        self.setFrame(False)
        self.setFocusPolicy(Qt.ClickFocus)
        self.last_text = contents

    def keyPressEvent(self, e):
        super(self.__class__, self).keyPressEvent(e)
        key = e.key()
        if key == Qt.Key_Return:
            self.last_text = self.text()
            self.clearFocus()
        if key == Qt.Key_Escape:
            self.setText(self.last_text)
            self.clearFocus()

    def setText(self, text):
        super(self.__class__, self).setText(text)
        self.last_text = text


class ProcessInstanceWidget(QFrame):

    """
    Widget in the Main Window to show and control a single process instance.
    This widget is responsible to start and finish one process, including its
    visualization manager.
    """

    closed = pyqtSignal()

    def __init__(self, process_class, parent=None):
        """Construct a process widget to control a single Process Instance.

        Keyword arguments:
        process_class -- DynamicalProcess class
        """
        super(self.__class__, self).__init__(parent)
        self._init_process(process_class)
        self._create_ui_elements()
        self._show_setup_dialog()
        self._init_visualization_manager()
        self._setup_layout()
        self._setup_ui_slots()

    def _init_process(self, process_class):
        self._process = process_class()
        self._setup_process_slots()

    def _init_visualization_manager(self):
        manager = VisualizationManager(self.views, self._process, self)
        self._view_manager = manager
        self.process.resumed.connect(self._view_manager.start)
        manager.show_all()

    def _create_ui_elements(self):
        self._title_editor = QInPlaceLineEdit()
        self._progress_bar = QProgressBar()
        self._button_start = QPushButton('Start')
        self._button_show = QPushButton('Show')
        self._button_close = QPushButton('Close')
        self._status_label = QLabel()
        self._button_start.next = self._process.start

    def _setup_layout(self):
        layout = QVBoxLayout()
        layout_buttons = QHBoxLayout()
        layout_buttons.addWidget(self._button_start)
        layout_buttons.addWidget(self._button_show)
        layout_buttons.addWidget(self._button_close)

        layout.addWidget(self._title_editor)
        layout.addWidget(self._progress_bar)
        layout.addLayout(layout_buttons)
        layout.addWidget(self._status_label)
        self.setLayout(layout)

    def _setup_ui_slots(self):
        self._title_editor.editingFinished.connect(self.on_title_edited)
        self._button_start.clicked.connect(self.button_start_click)
        self._button_show.clicked.connect(self.show_visualizations)
        self._button_close.clicked.connect(self.close_process)

    def _setup_process_slots(self):
        self.process.progress_changed.connect(self.set_progress)
        self.process.status_changed.connect(self.set_status)
        self.process.resumed.connect(self.state_resumed)
        self.process.paused.connect(self.state_paused)
        self.process.finished.connect(self.state_finished)
        self.process.title_changed.connect(self.on_title_changed)

    def _show_setup_dialog(self):
        """
        Asks user to setup the process and loads the process.
        Note it won't create a new instance before calling |on_load|.
        If the user has cancelled the process creation the initialization is
        aborted with an exception.
        """
        dialog = ProcessSetupDialog(self.process, self)
        if not dialog.exec_():
            raise UserHasAborted
        self.user_input = dialog.parameters.result
        self.views = dialog.views.result
        self._button_show.setEnabled(len(self.views) > 0)

    @pyqtSlot()
    def button_start_click(self):
        """Go to the next state."""
        self._button_start.next()

    @pyqtSlot()
    def close_process(self):
        if self.process.isRunning():
            self.process.terminate()
        self._view_manager.finalize()
        self.deleteLater()
        self.closed.emit()

    @pyqtSlot()
    def state_resumed(self):
        self._button_start.setText('Pause')
        self._button_start.next = self.process.pause

    @pyqtSlot()
    def state_paused(self):
        self._button_start.setText('Resume')
        self._button_start.next = self.process.resume

    @pyqtSlot()
    def state_finished(self):
        self._button_start.setText('Start')
        self._button_start.setEnabled(False)
        #self._title_editor.setEnabled(False)
        self.set_progress(100)
        self._status_label.setText('Finished and saved.')

    @pyqtSlot()
    def show_visualizations(self):
        self._view_manager.show_all()

    @pyqtSlot(int)
    def set_progress(self, value):
        self._progress_bar.setValue(value)

    @pyqtSlot(int)
    def set_status(self, value):
        self._status_label.setText(QString.fromUtf8(value))

    @pyqtSlot(QString)
    def on_title_changed(self, title):
        self._title_editor.setText(title)

    @pyqtSlot()
    def on_title_edited(self):
        self.process.set_title(self._title_editor.text())

    @property
    def process(self):
        return self._process


class ProcessManager(QWidget):

    process_finished = pyqtSignal()

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)
        self._instances = []
        self._create_ui_elements()
        self._setup_ui()
        self._setup_slots()

    def _create_ui_elements(self):
        # Create process
        self._process_chooser = ProcessChooser(self)

        # Control processes
        self._button_start_all = QPushButton('Start/Resume All')
        self._button_pause_all = QPushButton('Pause All')
        self._button_close_all = QPushButton('Close All')

        self._spinbox_epochs = QSpinBox()
        self._spinbox_epochs.setSuffix(' epochs/s')

        self._spinbox_vertices = QSpinBox()
        self._spinbox_vertices.setSuffix(' vertices/s')

        # View processes
        self._layout_processes = QVBoxLayout()
        self._layout_processes.setAlignment(Qt.AlignTop)
        self._layout_processes.setContentsMargins(0, 0, 0, 1)

    def _setup_ui(self):
        self._layout = QVBoxLayout()
        self._box_create_processes()
        self._box_control_processes()
        self._box_view_processes()

    def _box_create_processes(self):
        box = QGroupBox('Create')
        box.setLayout(QHBoxLayout())
        box.layout().addWidget(self._process_chooser)
        self._layout.addWidget(box)

    def _box_control_processes(self):
        box = QGroupBox('Control')
        box.setLayout(QHBoxLayout())
        box.layout().addWidget(self._spinbox_epochs)
        box.layout().addWidget(self._spinbox_vertices)
        box.layout().addWidget(self._button_start_all)
        box.layout().addWidget(self._button_pause_all)
        box.layout().addWidget(self._button_close_all)
        self._layout.addWidget(box)

    def _box_view_processes(self):
        box = QGroupBox('Processes')
        self._processes = QWidget()
        self._processes.setLayout(self._layout_processes)
        box.setLayout(QVBoxLayout())
        box.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        box.layout().addWidget(self._box_view_scroll())
        self._layout.addWidget(box)
        self.setLayout(self._layout)

    def _box_view_scroll(self):
        scroll_area = QScrollArea()
        scroll_area.setFrameStyle(QFrame.NoFrame)
        scroll_area.setAlignment(Qt.AlignJustify)
        scroll_area.setContentsMargins(0, 0, 0, 0)
        scroll_area.setWidget(self._processes)
        scroll_area.setWidgetResizable(True)
        return scroll_area

    def _setup_slots(self):
        self._process_chooser.process_created.connect(self.on_create_instance)

        self._spinbox_epochs.valueChanged.connect(self.on_update_epochs)
        self._spinbox_vertices.valueChanged.connect(self.on_update_vertices)

        self._button_start_all.clicked.connect(self.start_all)
        self._button_pause_all.clicked.connect(self.pause_all)
        self._button_close_all.clicked.connect(self.close_all)

    def _new_instance_widget(self, klass):
        w = ProcessInstanceWidget(klass, self)
        w.process.epochs_per_sec = self._spinbox_epochs.value()
        w.process.vertices_per_sec = self._spinbox_vertices.value()
        w.process.finished.connect(self.process_finished.emit)
        return w

    @pyqtSlot(type)
    def on_create_instance(self, klass):
        try:
            widget = self._new_instance_widget(klass)
            widget.closed.connect(lambda: self._remove_instance(widget))
            self._layout_processes.addWidget(widget)
            self._instances.append(widget)
        except UserHasAborted:
            pass

    def _remove_instance(self, instance):
        self._instances.remove(instance)

    @pyqtSlot(int)
    def on_update_epochs(self, speed):
        for instance in self._instances:
            instance.process.epochs_per_sec = speed

    @pyqtSlot(int)
    def on_update_vertices(self, speed):
        for instance in self._instances:
            instance.process.vertices_per_sec = speed

    @pyqtSlot()
    def close_all(self):
        for instance in self._instances:
            instance.close_process()

    @pyqtSlot()
    def start_all(self):
        self.on_update_epochs(self._spinbox_epochs.value())
        self.on_update_vertices(self._spinbox_vertices.value())
        for instance in self._instances:
            if not instance.process.isRunning():
                instance.button_start_click()
            instance.process.resume()

    @pyqtSlot()
    def pause_all(self):
        for instance in self._instances:
            instance.process.pause()
