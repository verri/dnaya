# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from PyQt4.QtCore import QString
from PyQt4.QtGui import QCheckBox, QHBoxLayout, QLabel, QPushButton,\
    QVBoxLayout, QWidget

from vtk import vtkIntArray, vtkGraphLayoutView, vtkLookupTable, vtkViewTheme
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from dnaya.ui.visualization.color import ColorLookupTable, \
    NetworkEdgesColors, NetworkVerticesColors


class NetworkVisualization(QWidget):

    name = 'Network'

    def __init__(self, process, parent=None):
        super(self.__class__, self).__init__(parent)
        self._process = process
        self._create_ui_elements()
        self._setup_ui()

    def _create_ui_elements(self):
        self._layout = QVBoxLayout()

        self._view = vtkGraphLayoutView()
        self._qvtk = QVTKRenderWindowInteractor(self,
                                                rw=self._view.GetRenderWindow())

        self._layout_buttons = QHBoxLayout()
        self._button_force_layout = QPushButton('Force Directed')
        self._button_random_layout = QPushButton('Random')

    def _setup_ui(self):
        self._layout.addWidget(self._qvtk)
        self._layout_buttons.addWidget(self._button_force_layout)
        self._layout_buttons.addWidget(self._button_random_layout)
        self._layout.addWidget(QLabel('Layout'))
        self._layout.addLayout(self._layout_buttons)

        self._button_force_layout.clicked.connect(self.force_directed_layout)
        self._button_random_layout.clicked.connect(self.random_layout)

        self._setup_view()
        self.setLayout(self._layout)

    def _setup_view(self):
        self._view.SetInteractor(self._qvtk.GetRenderWindow().GetInteractor())
        self._view.SetInteractionModeTo3D()
        self._view.SetRepresentationFromInput(self._process.network.vtk)
        self._setup_coloring()
        self._setup_strategy()
        self._setup_theme()

        self._view.ResetCamera()
        self.show()
        self._view.GetInteractor().Initialize()
        self._view.GetInteractor().Start()

    def _setup_coloring(self):
        self._color_table = ColorLookupTable()

        self.vcolors = NetworkVerticesColors(self._process.network,
                                             self._color_table)
        self._view.SetVertexColorArrayName(self.vcolors.col_name)
        self._view.ColorVerticesOn()

        # self.ecolors = NetworkEdgesColors(self._process.network,
        #        self._color_table)
        # self._view.SetEdgeColorArrayName(self.ecolors.col_name)
        # self._view.ColorEdgesOn()

    def _setup_strategy(self):
        self._view.SetLayoutStrategyToPassThrough()
        if len(self._process.network) <= 350:
            self.force_directed_layout()

    def _setup_theme(self):
        self._theme = vtkViewTheme()
        self._theme.SetPointLookupTable(self._color_table.table)
        self._theme.SetScalePointLookupTable(False)
        # self._theme.SetLineWidth(2.0)
        self._view.ApplyViewTheme(self._theme)

    def finalize(self):
        self._qvtk.Finalize()

    def refresh(self):
        self.vcolors.update()
        # self.ecolors.update()
        self._qvtk.Render()

    def force_directed_layout(self):
        self._view.SetLayoutStrategyToForceDirected()
        strategy = self._view.GetLayoutStrategy()
        strategy.SetThreeDimensionalLayout(True)
        strategy.SetMaxNumberOfIterations(100)
        strategy.SetIterationsPerLayout(100)
        strategy.SetAutomaticBoundsComputation(False)
        strategy.SetGraphBounds(0, 0, 0, 1, 1, 1)
        self._view.ResetCamera()
        self.refresh()

    def random_layout(self):
        self._view.SetLayoutStrategyToRandom()
        self._view.GetLayoutStrategy().SetThreeDimensionalLayout(True)
        self._view.ResetCamera()
        self.refresh()


class NetworkFactory(object):

    """
    A NetworkVisualization factory.

    A factory is instanced by the visualization manager, which will ask
    for a widget from |make_widget| to be added to its form.

    When the user confirms, |build_visualizations| is called to instantiate
    all visualizations to a list.

    Afterwards, the visualization manager will iterate through |self| to
    instantiate each visualization in its floating dialog.
    """

    label = 'Network'
    default = True
    tip = 'Show network during the process execution.'

    def __init__(self, process, parent=None):
        self._parent = parent
        self.process = process
        self.widgets = list()

    def __iter__(self):
        return self.widgets.__iter__()

    def build_visualizations(self):
        """Create instances according to what the user has requested."""
        if self.checkbox.isChecked():
            nv = NetworkVisualization(self.process)
            self.widgets.append(nv)

    def get_setup_widget(self):
        """Returns a widget to be added in the Visualizations form."""
        checkbox = QCheckBox(self._parent)
        checkbox.setToolTip(QString.fromUtf8(self.tip))
        checkbox.setText(QString.fromUtf8(self.label))
        checkbox.setChecked(self.default)
        self.checkbox = checkbox
        return checkbox
