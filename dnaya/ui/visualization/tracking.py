# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from PyQt4.QtCore import pyqtSlot, Qt
from PyQt4.QtGui import QCheckBox, QDialog, QDialogButtonBox, QGroupBox,\
    QHBoxLayout, QLabel, QPushButton, QVBoxLayout, QWidget

from vtk import vtkAxis, vtkChart, vtkChartXY, vtkContextView, vtkFloatArray,\
    vtkTable
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor


class TrackingVisualization(QWidget):

    name = 'Property tracking'

    def __init__(self, process, properties):
        super(self.__class__, self).__init__()

        self._properties = properties
        self._network = process.network

        process.epoch_processed.connect(self.epoch)

        self._create_data_elements()
        self._create_ui_elements()
        self._setup_ui()

    def _create_data_elements(self):
        self._table = vtkTable()

        array = vtkFloatArray()
        array.SetName('epoch')
        self._table.AddColumn(array)

        for name in self._properties:
            array = vtkFloatArray()
            array.SetName(name)
            self._table.AddColumn(array)

    def _create_ui_elements(self):
        self._view = vtkContextView()
        self._qvtk = QVTKRenderWindowInteractor(self,
                                                rw=self._view.GetRenderWindow())
        self._chart = vtkChartXY()

    def _setup_ui(self):
        layout = QVBoxLayout()
        layout.addWidget(self._qvtk)

        self._view.SetInteractor(self._qvtk.GetRenderWindow().GetInteractor())
        self._view.GetScene().AddItem(self._chart)
        self._chart.SetShowLegend(True)
        self._chart.GetAxis(vtkAxis.LEFT).SetTitle('value')
        self._chart.GetAxis(vtkAxis.BOTTOM).SetTitle('epoch')

        self.setLayout(layout)

        self.show()
        self._view.GetInteractor().Initialize()
        self._view.GetInteractor().Start()

    @pyqtSlot(int)
    def epoch(self, e):
        values = [self._network[name] for name in self._properties]
        self.update_values(values)

    def finalize(self):
        self._qvtk.Finalize()

    def refresh(self):
        self._qvtk.Render()

    def update_values(self, values):
        t = self._table.GetNumberOfRows()
        if t == 1:
            for i in xrange(1, self._table.GetNumberOfColumns()):
                plot = self._chart.AddPlot(vtkChart.LINE)
                plot.SetInputData(self._table, 0, i)

        self._table.InsertNextBlankRow()

        self._table.SetValue(t, 0, t)
        for i in xrange(len(values)):
            self._table.SetValue(t, i + 1, values[i])

        self._table.Modified()
        self._chart.RecalculateBounds()


class TrackingSelection(QWidget):

    """ Widget that holds and displays the selected properties to be
    tracked.
    """

    def __init__(self, selection, parent=None):
        super(self.__class__, self).__init__(parent)

        self.selection = selection

        self.label = QLabel(','.join(selection), self)
        self.button = QPushButton('Remove')

        layout = QHBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.button)

        self.setLayout(layout)


class TrackingChooser(QGroupBox):

    """ Widget to be added in the Visualization Form. """

    def __init__(self, network, parent=None):
        super(self.__class__, self).__init__(parent)

        self._selections = []
        self._network = network

        self._create_ui_elements()
        self._setup_ui()

    def _create_ui_elements(self):
        self._button_choose = QPushButton('...')
        self._layout = QVBoxLayout()

    def _setup_ui(self):
        self.setTitle('Tracking properties')

        self._button_choose.clicked.connect(self.choose)

        self._layout.addWidget(self._button_choose)
        self.setLayout(self._layout)

    def _dialog(self):
        """ Returns the dialog that contains all network properties. """
        dialog = QDialog(self)
        layout = QVBoxLayout()

        dialog.options = list()
        for name in self._network.data.iterkeys():
            cb = QCheckBox(name, dialog)
            dialog.options.append(cb)
            layout.addWidget(cb)

        buttons = QDialogButtonBox(QDialogButtonBox.Ok, Qt.Horizontal, dialog)
        buttons.accepted.connect(dialog.accept)
        layout.addWidget(buttons)

        dialog.setModal(True)
        dialog.setLayout(layout)
        dialog.setWindowTitle('Select properties')
        return dialog

    @pyqtSlot()
    def choose(self):
        dialog = self._dialog()
        dialog.exec_()

        properties = [str(checkbox.text()) for checkbox in dialog.options
                      if checkbox.isChecked()]

        if properties:
            ts = TrackingSelection(properties, self)
            self._selections.append(ts)
            self._layout.addWidget(ts)
            ts.button.clicked.connect(lambda: self.remove(ts))

    def remove(self, ts):
        """ Slot. """
        self._layout.removeWidget(ts)
        self._selections.remove(ts)
        ts.deleteLater()
        del ts

    def selections(self):
        l = list()
        for widget in self._selections:
            l.append(widget.selection)
        return l


class TrackingFactory(object):

    label = 'Property tracking'

    def __init__(self, process, parent=None):
        self._parent = parent
        self._process = process
        self._visualizations = list()
        self._setup_widget = None

    def __iter__(self):
        return self._visualizations.__iter__()

    def build_visualizations(self):
        if self._setup_widget is not None and self._setup_widget.selections():
            for selection in self._setup_widget.selections():
                self._visualizations.append(
                    TrackingVisualization(self._process, selection))

    def get_setup_widget(self):
        if self._process.network.data:
            self._setup_widget = TrackingChooser(
                self._process.network, self._parent)
            return self._setup_widget
