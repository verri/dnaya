# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from numpy import histogram

from PyQt4.QtCore import pyqtSlot, QString
from PyQt4.QtGui import QCheckBox, QVBoxLayout, QWidget

from vtk import vtkChart, vtkChartXY, vtkContextView, vtkFloatArray, \
    vtkTable
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from dnaya.task import TaskContext


def compute_vertices_degrees(network):
    degrees = list()
    for v in network:
        degrees.append(len(v.neighbors()))
    return degrees


def task_histogram(network):
    x, y = histogram(compute_vertices_degrees(network))
    return x * 1.0 / sum(x)


class HistogramVisualization(QWidget):

    name = 'Histogram'

    def __init__(self, process, parent=None):
        super(self.__class__, self).__init__(parent)
        self.process = process
        self.task = None

        self._create_ui_elements()
        self._create_data_elements()
        self._setup_ui()

    def _create_ui_elements(self):
        self.view = vtkContextView()
        self.qvtk = QVTKRenderWindowInteractor(self,
                                               rw=self.view.GetRenderWindow())
        self.chart = vtkChartXY()

    def _add_column(self, name):
        array = vtkFloatArray()
        array.SetName(name)
        self.table.AddColumn(array)

    def _create_data_elements(self):
        self.table = vtkTable()
        self._add_column('degree')
        self._add_column('fraction of vertices')

    def _setup_ui(self):
        layout = QVBoxLayout()
        layout.addWidget(self.qvtk)
        self.view.SetInteractor(self.qvtk.GetRenderWindow().GetInteractor())
        self.view.GetInteractor().Start()
        self.view.GetScene().AddItem(self.chart)
        self.chart.SetShowLegend(True)
        self.setLayout(layout)
        self.show()
        self.view.GetInteractor().Initialize()
        self.view.GetInteractor().Start()
        plot = self.chart.AddPlot(vtkChart.BAR)
        plot.SetInputData(self.table, 0, 1)

    def finalize(self):
        self.qvtk.Finalize()

    def refresh(self):
        self.qvtk.Render()
        if not self.task:
            self.task = TaskContext(task_histogram)
            self.task.done.connect(self.update_histogram)
            self.process.add_task(self.task)

    @pyqtSlot()
    def update_histogram(self):
        x = self.task.result
        self.task = None

        rows = len(x)
        self.table.SetNumberOfRows(rows)
        for r in range(rows):
            self.table.SetValue(r, 0, r - 0.5)
            self.table.SetValue(r, 1, x[r])
        self.table.Modified()
        self.chart.GetAxis(1).SetNumberOfTicks(rows + 1)
        self.chart.GetAxis(0).SetRange(0, max(x))
        self.chart.GetAxis(1).SetRange(0, rows)


class HistogramFactory(object):

    label = 'Vertices degree histogram'
    default = True
    tip = 'Show vertices degree histogram.'

    def __init__(self, process, parent=None):
        self._parent = parent
        self.process = process
        self.widgets = list()

    def __iter__(self):
        return self.widgets.__iter__()

    def build_visualizations(self):
        """Create instances according to what the user has requested."""
        if self.checkbox.isChecked():
            nv = HistogramVisualization(self.process)
            self.widgets.append(nv)

    def get_setup_widget(self):
        """Returns a widget to be added in the Visualizations form."""
        checkbox = QCheckBox(self._parent)
        checkbox.setToolTip(QString.fromUtf8(self.tip))
        checkbox.setText(QString.fromUtf8(self.label))
        checkbox.setChecked(self.default)
        self.checkbox = checkbox
        return checkbox
