# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from time import time

from PyQt4.QtCore import pyqtSlot, QString, Qt, QTimer
from PyQt4.QtGui import QDialog, QFormLayout, QFrame, QLabel, QVBoxLayout,\
    QWidget

from dnaya.process import compute_delay
from dnaya.ui.visualization.histogram import HistogramFactory
from dnaya.ui.visualization.network import NetworkFactory
from dnaya.ui.visualization.tracking import TrackingFactory


class FloatingVisualization(QDialog):

    """
    Holds a visualization widget in a floating dialog.  An instance will
    run separately from both the main window and the process thread.
    """

    def __init__(self, widget, parent=None):
        super(self.__class__, self).__init__(parent)

        self.setWindowFlags(Qt.Window)
        self.setWindowTitle(widget.name)
        self._widget = widget

        self._create_ui_elements()
        self._setup_ui()

    def _create_ui_elements(self):
        self.layout = QVBoxLayout()

    def _create_title(self):
        title = QLabel()
        title.setAlignment(Qt.AlignHCenter)
        self._title_label = title
        self.layout.addWidget(title)

        sep = QFrame()
        sep.setFrameShape(QFrame.HLine)
        sep.setFrameShadow(QFrame.Sunken)
        self.layout.addWidget(sep)

    def _setup_ui(self):
        self._create_title()
        self.layout.addWidget(self._widget)
        self.setLayout(self.layout)

    def finalize(self):
        self._widget.finalize()

    def set_title(self, value):
        self._title_label.setText('<b>' + value + '</b>')

    def refresh(self):
        self._widget.refresh()


class VisualizationManager(QWidget):

    def __init__(self, views, process, parent=None):
        '''Manage instances of |views| classes as visualizations, updating
        each one at most |fps| times per second.'''
        super(self.__class__, self).__init__(parent)
        self._fps = 1
        self._process = process
        self._running = False
        self._create_widgets(views)
        self._setup_signals()

    def _create_widgets(self, views):
        vis = list()
        name = self._process.title
        for view in views:
            v = FloatingVisualization(view, self)
            v.set_title(name)
            vis.append(v)
        self._visualizations = vis

    def _setup_signals(self):
        process = self._process
        process.title_changed.connect(self.update_title)
        process.resumed.connect(self.start)
        process.paused.connect(self.stop)
        process.finished.connect(self.finish)

    def finalize(self):
        self.stop()
        for v in self._visualizations:
            v.finalize()

    def finish(self):
        self.stop()
        self.refresh()

    def schedule(self, start=0, end=0):
        if self._running:
            delay = compute_delay(self._fps, start, end)
            QTimer.singleShot(delay, self.refresh)

    def show_all(self):
        for visualization in self._visualizations:
            visualization.show()
            visualization.raise_()

    @pyqtSlot()
    def start(self):
        if not self._running and self._visualizations:
            self._running = True
            self.schedule()

    @pyqtSlot()
    def stop(self):
        self._running = False

    def refresh(self):
        start = time()
        for visualization in self._visualizations:
            if visualization.isVisible():
                visualization.refresh()
        end = time()
        self.schedule(start, end)

    def refresh_visualizations(self):
        for visualization in self._visualizations:
            if visualization.isVisible():
                visualization.refresh()

    @pyqtSlot(QString)
    def update_title(self, title):
        for visualization in self._visualizations:
            visualization.set_title(title)


class VisualizationForm(QFormLayout):

    FACTORIES = [NetworkFactory, HistogramFactory, TrackingFactory]

    def __init__(self, process, parent=None):
        super(self.__class__, self).__init__(parent)
        self.process = process
        self.setVerticalSpacing(10)
        self.setHorizontalSpacing(5)
        self._create_factories()
        self._setup_ui()

    def __str__(self):
        return 'Visualizations'

    def _create_factories(self):
        self.factories = list()
        for factory in self.FACTORIES:
            self.factories.append(factory(self.process))

    def _setup_ui(self):
        for factory in self.factories:
            widget = factory.get_setup_widget()
            if widget:
                self.addRow(widget)

    def accept(self):
        self.result = list()
        for factory in self.factories:
            factory.build_visualizations()
            for widget in factory:
                self.result.append(widget)
        return True
