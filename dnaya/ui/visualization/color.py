# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from vtk import vtkIntArray, vtkLookupTable, vtkVariant, vtkVariantArray

COLORS = {
    0: {'name': 'white',  'rgb': [1.0, 1.0, 1.0]},
    1: {'name': 'red',    'rgb': [1.0, 0.0, 0.0]},
    2: {'name': 'green',  'rgb': [0.0, 1.0, 0.0]},
    3: {'name': 'blue',   'rgb': [0.0, 0.0, 1.0]},
    4: {'name': 'yellow', 'rgb': [1.0, 1.0, 0.0]},
    5: {'name': 'cyan',   'rgb': [0.0, 0.6, 1.0]}
}


class ColorLookupTable(object):

    def __init__(self):
        self.names = dict()
        self.table = vtkLookupTable()
        self._setup_colors()

    def _setup_colors(self, color_table=COLORS):
        self.table.SetIndexedLookup(True)
        self.table.SetNanColor(0.0, 0.0, 0.0, 1.0)
        for index, c in color_table.iteritems():
            self.table.SetAnnotation(vtkVariant(index), c['name'])
        self.table.SetRange(min(color_table), max(color_table))
        self.table.Build()
        for index, color in color_table.iteritems():
            self.table.SetTableValue(index, *color['rgb'])
            self.names[color['name']] = index

    def get(self, key, default=None):
        if key in self.names:
            return self.names[key]
        return default


class NetworkColors(object):

    def __init__(self, network, table):
        self.network = network
        self.table = table
        self._create_array()

    def _create_array(self):
        self.array = vtkVariantArray()
        # self.array.SetLookupTable(self.table.table)
        self.array.SetNumberOfComponents(1)
        self.array.SetName(self.col_name)
        self.data().AddArray(self.array)

    def get_color_index(self, obj):
        value = obj.get('color', 0)
        if isinstance(value, str):
            value = self.table.get(value, 0)
        return vtkVariant(value)

    def data(self):
        pass

    def update(self):
        pass


class NetworkVerticesColors(NetworkColors):

    col_name = 'vcolor'

    def __init__(self, network, table):
        super(self.__class__, self).__init__(network, table)
        self.array.SetNumberOfValues(len(self.network))

    def data(self):
        return self.network.vtk.GetVertexData()

    def update(self):
        if not self.network.vertices:
            return
        self.array.SetNumberOfValues(len(self.network))
        for v in self.network:
            color = self.get_color_index(v)
            self.array.SetValue(v.vid, color)


class NetworkEdgesColors(NetworkColors):

    col_name = 'ecolor'

    def __init__(self, network, table):
        super(self.__class__, self).__init__(network, table)
        self.array.SetNumberOfValues(len(network))

    def data(self):
        return self.network.vtk.GetEdgeData()

    def update(self):
        edges = self.network.edges.copy()
        if not edges:
            return
        self.array.SetNumberOfValues(len(edges))
        for e in edges:
            if e not in self.network:
                continue
            edge = self.network[e]
            color = self.get_color_index(edge)
            self.array.SetValue(edge.eid, color)
