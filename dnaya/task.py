# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from PyQt4.QtCore import pyqtSignal, QObject


class TaskContext(QObject):

    """
    A task context holds a function task to be called by a process.  When
    a widget wants a function to be executed safely (i.e. no race condition
    when accessing the network), the widget should add a task to the process
    queue:

    def my_task(network, a, b):
        # work
        return results

    # In the widget:
    self.task = TaskContext(my_task, (arg1, arg2))
    self.task.done.connect(self.collect_task_result)
    self.process.add_task(self.task)

    In self.collect_task_result, my_task(network, arg1, arg2) result will be
    available in self.task.result.

    Notice my_task will always receive at least one parameter, the network.
    """

    done = pyqtSignal()

    def __init__(self, function=None, args=()):
        super(self.__class__, self).__init__()
        self.function = function
        self.args = args

    def set_network(self, network):
        self.network = network

    def run(self):
        self.result = self.function(self.network, *self.args)
