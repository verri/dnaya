# -*- coding: utf-8 -*-
# Copyright © 2014, Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

import sys
import os
from importlib import import_module
from fnmatch import fnmatch

from dnaya.config import Settings
from dnaya.process import DynamicalProcess
from dnaya.storage import FileFormatError, NetworkStorage


class PluginLoader(object):

    """
    Reads a directory (as a package) and loads all python files in search of
    new plugins.  Plugins are identified as subclasses of DynamicalProcess.
    """

    def __init__(self, package=None):
        if package is not None:
            self._read_directory(package)
        else:
            settings = Settings()
            for package in settings.get('plugin_directories'):
                self._read_directory(package)

    def __iter__(self):
        return DynamicalProcess.__subclasses__().__iter__()

    def _read_directory(self, package):
        if not os.path.isdir(package):
            return

        sys.path.append(package)
        for fn in os.listdir(package):
            if fnmatch(fn, '*.py'):
                import_module(fn[:-3])
        sys.path.pop()


class NetworkLoader(object):

    """
    Reads all files from a directory searching for network files.  A network
    file is identified by its signature rather than its extension.
    """

    def __init__(self, directory=None):
        self._networks = []
        if directory is not None:
            self._read_directory(directory)
        else:
            settings = Settings()
            for directory in settings.get('network_directories'):
                self._read_directory(directory)

    def __iter__(self):
        return self._networks.__iter__()

    def _read_directory(self, directory):
        if not os.path.isdir(directory):
            return
        for filename in os.listdir(directory):
            fname = os.path.join(directory, filename)
            if os.path.isfile(fname):
                self._read_network(fname)

    def _read_network(self, filename):
        storage = NetworkStorage()
        try:
            storage.load(filename, header_only=True)
            self._networks.append(storage)
        except FileFormatError:
            pass
