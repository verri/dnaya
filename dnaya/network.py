# -*- coding: utf-8 -*-
# Copyright © 2014, Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.
"""Network and Agent abstraction layer for the plugin API"""

from vtk import vtkMutableDirectedGraph, vtkMutableUndirectedGraph,\
    vtkInEdgeIterator, vtkOutEdgeIterator


class Network(object):

    """Network representation.

    network = Network(n=10, directed=False)

    data = network[0]     # Access vertex data

    data = network[0, 1]  # Access the edge's data
    del network[0, 1]     # Delete the edge (0, 1)

    if 0 in network:      # Check if vertex exists
    if (0, 1) in network  # Check if an edge exists

    len(network)          # How many vertices exists
    """

    def __init__(self, n=0, directed=False):
        self._create_vtk_graph(directed)
        self._data = dict()
        self._vertices = NetworkDict()
        self._edges = NetworkDict()
        self.add_vertices(n)

    def __contains__(self, key):
        if isinstance(key, int):
            return key in self._vertices
        if isinstance(key, tuple):
            A = key in self._edges
            B = not self.directed and (key[::-1] in self._edges)
            return A or B
        if isinstance(key, str):
            return key in self._data
        raise KeyError('Expected either vertex, edge or property.')

    def __delitem__(self, key):
        # When a vertex/edge is removed from VTK, the operation takes
        # either O(|V|) or O(|E|), because it reindexes all vertices/edges.
        # As it would break the consistency of our internal representation,
        # this operation is not available.  Also, during the process execution,
        # the graph traversal would break when a on_vertex_update at vertex i
        # deletes a vertex j > i.  It would subsequently call a vertex update
        # to a deleted vertex.
        if isinstance(key, tuple):
            e = self._edges[key]
            self._vtk_graph.RemoveEdge(e.eid)
            del self._edges[key]
        elif isinstance(key, str):
            del self._data[key]
        else:
            raise NotImplementedError

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._vertices[item]
        if isinstance(item, tuple):
            if self.directed or item in self._edges:
                return self._edges[item]
            return self._edges[item[::-1]]
        if isinstance(item, str):
            return self._data[item]
        raise KeyError

    def __setitem__(self, item, value):
        if isinstance(item, str):
            self._data[item] = value
        else:
            raise AttributeError

    def __iter__(self):
        return self._vertices.values().__iter__()

    def __len__(self):
        return len(self.vertices)

    def __str__(self):
        details = 'vertices=%d edges=%d' % (len(self), len(self.edges))
        return '%s (directed=%r %s)' % (self.__class__, self.directed, details)

    def _create_vtk_graph(self, value):
        self._directed = value
        VTK = (vtkMutableUndirectedGraph, vtkMutableDirectedGraph)[value]
        self._vtk_graph = VTK()

    def add_edge(self, vid1, vid2):
        if vid1 not in self or vid2 not in self:
            raise AttributeError('Both vertices must exist in the network.')
        if (vid1, vid2) in self or (not self.directed and (vid2, vid1) in self):
            raise AttributeError("Network doesn't support multi-edges.")
        edge = NetworkEdge(vid1, vid2, self)
        self._edges[vid1, vid2] = edge
        return edge

    def add_edges(self, edge_list):
        for v1, v2 in edge_list:
            self.add_edge(v1, v2)

    def add_vertex(self):
        """Returns a new instance of NetworkVertex"""
        # VTK's AddVertex() returns a 'long', whereas we use 'int'.
        v = NetworkVertex(network=self)
        self._vertices[v.vid] = v
        return v

    def add_vertices(self, n):
        for i in range(n):
            self.add_vertex()

    @property
    def edges(self):
        return self._edges

    @property
    def vertices(self):
        return self._vertices

    @property
    def directed(self):
        return self._directed

    @property
    def vtk(self):
        return self._vtk_graph

    @property
    def data(self):
        return self._data


class NetworkDict(dict):

    """
    Extends the dictionary to allow key-wise attribute assignments.

    network.vertices['color'] = 'white'
    network.edges['weight'] = 0
    """

    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)

    def __delitem__(self, key):
        if isinstance(key, str):
            for k in self.values():
                del k[key]
        else:
            dict.__delitem__(self, key)

    def __setitem__(self, key, value):
        if isinstance(key, str):
            for k in self.values():
                k[key] = value
        else:
            dict.__setitem__(self, key, value)


class NetworkVertex(object):

    """
    A network vertex.

    vertex['weight'] = 10     # Set an attribute
    'weight' in vertex        # Check an attribute
    del vertex['weight']      # Delete an attribute
    vertex.edges('all')       # Get the incident edges list
    vertex.neighbors('all')   # Get the adjacent vertices set
    """

    def __init__(self, network=None, vid=None):
        self._network = network
        self._data = dict()
        if vid is not None:
            self._vid = vid
        elif network is not None:
            self._vid = int(network._vtk_graph.AddVertex())
        else:
            raise ValueError('Expected either network or vid.')

    def __contains__(self, key):
        return key in self._data

    def __delitem__(self, key):
        del self._data[key]

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value

    def __str__(self):
        return "%s (vid=%d data=%r)" % (self.__class__, self.vid, self.data)

    def _edges(self, fn, vtk_iterator_init, vtk_iterator):
        adj = list()
        it = vtk_iterator()
        vtk_iterator_init(self.vid, it)
        while it.HasNext():
            e = it.NextGraphEdge()
            source = int(e.GetSource())
            target = int(e.GetTarget())
            pair = (source, target)
            if not self.network.directed and pair not in self.network:
                pair = (target, source)
            edge = self.network[pair]
            adj.append(fn(edge))
        return adj

    def edges(self, mode='all', fn=lambda edge: edge):
        if mode not in ['all', 'in', 'out']:
            raise ValueError('Mode must be one of [all, in, out].')
        if not self.network:
            return list()
        if not self.network.directed:
            return self._edges(fn, self.network.vtk.GetInEdges,
                               vtkInEdgeIterator)
        l = list()
        if mode in ['in', 'all']:
            l += self._edges(fn, self.network.vtk.GetInEdges,
                             vtkInEdgeIterator)
        if mode in ['out', 'all']:
            l += self._edges(fn, self.network.vtk.GetOutEdges,
                             vtkOutEdgeIterator)
        return l

    def get(self, key, default=None):
        if key in self.data:
            return self.data[key]
        return default

    def _neighbors(self, vtk_iterator_init, vtk_iterator):
        nei = set()
        it = vtk_iterator()
        vtk_iterator_init(self.vid, it)
        while it.HasNext():
            e = it.NextGraphEdge()
            source = int(e.GetSource())
            target = int(e.GetTarget())
            vid = source if source != self.vid else target
            nei.add(self.network[vid])
        return nei

    def neighbors(self, mode='all'):
        if mode not in ['all', 'in', 'out']:
            raise ValueError('Mode must be one of [all, in, out].')
        if not self.network:
            return set()
        if not self.network.directed:
            return self._neighbors(self.network.vtk.GetInEdges,
                                   vtkInEdgeIterator)
        l = set()
        if mode in ['in', 'all']:
            l |= self._neighbors(self.network.vtk.GetInEdges,
                                 vtkInEdgeIterator)
        if mode in ['out', 'all']:
            l |= self._neighbors(self.network.vtk.GetOutEdges,
                                 vtkOutEdgeIterator)
        return l

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        """Ensure that |data| is always a dictionary."""
        if not isinstance(value, dict):
            raise ValueError('The vertex data must be a dict')
        self._data = value

    @property
    def network(self):
        return self._network

    @property
    def vid(self):
        return self._vid


class NetworkEdge(object):

    """A network edge.

    edge['weight'] = 10     # Set an attribute
    'weight' in edge        # Check an attribute
    del edge['weight']      # Delete an attribute
    vid1, vid2 = edge.pair  # Get the vertices ids
    """

    def __init__(self, vid1, vid2, network):
        self._data = dict()
        self._network = network
        self._edge = network.vtk.AddGraphEdge(vid1, vid2)
        self._pair = (vid1, vid2)

    def __contains__(self, key):
        return key in self._data

    def __delitem__(self, key):
        del self._data[key]

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value

    def __str__(self):
        return "%s (edge=%r data=%r)" % (self.__class__, self.pair, self.data)

    def get(self, key, default=None):
        if key in self.data:
            return self.data[key]
        return default

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        """To keep this class methods working, we need to ensure that
        the |self._data| will always be a dictionary."""
        if not isinstance(value, dict):
            raise ValueError('The vertex data must be a dict')
        self._data = value

    @property
    def eid(self):
        return int(self._edge.GetId())

    @property
    def pair(self):
        return self._pair

    @property
    def network(self):
        return self._network
