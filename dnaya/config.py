# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from os.path import join
from ast import literal_eval

from PyQt4.Qt import QDesktopServices as Services
from PyQt4.QtCore import QSettings, QString

ORGANIZATION_NAME = 'USP'
PROGRAM_NAME = 'DNAYA'

PROGRAM_PATH = '.'  # TODO installation location
USER_PATH = str(Services.storageLocation(Services.HomeLocation))

DATA_DIR = '.dnaya'
NETWORKS_DIR = 'networks'
PLUGINS_DIR = 'plugins'

DEFAULTS = {'network_directories':
            [join(USER_PATH, DATA_DIR, NETWORKS_DIR),
             join(PROGRAM_PATH, NETWORKS_DIR)],
            'plugin_directories': [join(PROGRAM_PATH, PLUGINS_DIR)]}


class Settings:

    def __init__(self):
        self._qsettings = QSettings(ORGANIZATION_NAME, PROGRAM_NAME)

    def __getitem__(self, key):
        if key not in self:
            raise KeyError
        return literal_eval(str(self._qsettings.value(key).toString()))

    def __setitem__(self, key, value):
        self._qsettings.setValue(key, QString(repr(value)))

    def __delitem__(self, key):
        self._qsettings.remove(key)

    def __contains__(self, key):
        return self._qsettings.contains(key)

    def get(self, key, default=None):
        if key in self:
            return self[key]
        if default is None and key in DEFAULTS:
            value = DEFAULTS[key]
            self[key] = value
            return value
        return default

    def pop(self, key, default=None):
        if key in self:
            value = self[key]
            self._qsettings.remove(key)
            return value
        if default is None:
            raise KeyError
        return default
