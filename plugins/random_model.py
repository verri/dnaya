# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from math import ceil
from random import random

from PyQt4.QtCore import QTimer

from dnaya.api import DynamicalProcess, Network

REQUEST_FORM = {
    'N': {'type': 'integer',
          'default': 100,
          'min': 2,
          'tip': 'Total number of agents in the network.'},

    'M': {'type': 'integer',
          'default': 0,
          'tip': 'Maximum number of links to create. M = 0 means no limit.'},

    'p': {'type': 'real',
          'step': 0.01,
          'default': 0.05,
          'tip': 'Probability of creating a link.'}
}


class ErdosRenyiModel(DynamicalProcess):
    author = 'Verri'
    menu = 'Random Models/Erdős–Rényi'
    description = 'Erdős–Rényi random graph model.'

    def __init__(self, parent=None):
        DynamicalProcess.__init__(self, parent)

    def request_form(self):
        return REQUEST_FORM

    def on_load(self, form):
        self.assign_form(form)
        self.max_edges = self.N * (self.N - 1) * 0.5
        self.network = Network(self.N)
        inf = lambda x: x if x else '∞'
        self.set_title('Erdős–Rényi (N={}, M={}, p={})'.format(
            self.N, inf(self.M), inf(self.p)))
        self.last_vid = 0
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_status)

        self.network['density'] = 0

    def on_update_vertex(self, vid):
        self.last_vid = vid
        for j in xrange(vid + 1, self.N):
            if random() < self.p and (vid, j) not in self.network:
                self.network.add_edge(vid, j)

    def on_update_epoch(self, epoch_id):
        if epoch_id == 0:
            self.timer.start(200)
        else:
            self.timer.stop()
            raise StopIteration

    def update_status(self):
        vid = self.last_vid
        self.set_progress(ceil(100 * (vid + 1) / self.N))
        self.set_status('Updating vertex {}. {}'.format(vid, self.edge_info()))

    def edge_info(self):
        e = len(self.network.edges)
        density = e / self.max_edges
        self.network['density'] = density
        return 'Edges: %d (Net density: %.2f%%)' % (e, density)
