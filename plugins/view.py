# -*- coding: utf-8 -*-
# Copyright © 2014, Paulo Urio and Filipe Verri.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from dnaya.api import DynamicalProcess

REQUEST_FORM = {
    'network': {'type': 'network'},

    'layout': {'type': 'string',
               'default': 'fr3d'}
}


class ViewNetwork(DynamicalProcess):
    menu = 'Visualisers/Network'
    author = 'Verri'
    description = 'Network visualiser according to a layout.'

    def __init__(self):
        DynamicalProcess.__init__(self)

    def request_form(self):
        return REQUEST_FORM

    def on_load(self, form):
        self.form = form

    def on_update_epoch(self, epoch_id):
        raise StopIteration

    def on_update_vertex(self, vid):
        pass
