# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from math import ceil
from dnaya.api import DynamicalProcess, Network

import numpy as np

from PyQt4.QtCore import QTimer

REQUEST_FORM = {
    'N': {'type': 'integer',
          'default': 100,
          'min': 2,
          'tip': 'Total number of agents in the network.'},

    'K': {'type': 'integer',
          'default': 10,
          'min': 1,
          'tip': 'An agent <i>i</i> will possibly connect to vertices with indexes from i-K to i+K.'},

    'p': {'type': 'real',
          'step': 0.01,
          'default': 0.01,
          'min': 0.001,
          'tip': 'Probability of creating one link.'}
}


class NormalErdosRenyiModel(DynamicalProcess):
    author = 'Urio'
    menu = 'Random Models/Erdős–Rényi + normal neighborhood'
    description = 'Erdős–Rényi random graph model with neighbhorhood of up to K agents.'

    def __init__(self, parent=None):
        DynamicalProcess.__init__(self, parent)

    def request_form(self):
        return REQUEST_FORM

    def on_load(self, form):
        self.assign_form(form)
        self.max_edges = self.N * (self.N - 1) * 0.5
        self.network = Network(self.N)
        self.set_title('Normal Erdős–Rényi (N={}, K={}, p={})'.format(
            self. N, self.K, self.p))
        self.last_vid = 0
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_status)

    def on_update_vertex(self, vid):
        j = int(np.random.normal(vid, self.K))
        j = min(max(0, j), self.N - 1)
        self.last_vid = vid
        if (vid, j) not in self.network:
            self.network.add_edge(vid, j)

    def on_update_epoch(self, epoch_id):
        if epoch_id == 0:
            self.timer.start(200)
        else:
            self.timer.stop()
            raise StopIteration

    def update_status(self):
        vid = self.last_vid
        self.set_progress(ceil(100 * (vid + 1) / self.N))
        self.set_status('Updating vertex {}. {}'.format(vid, self.edge_info()))

    def edge_info(self):
        e = len(self.network.edges)
        density = 100 * e / self.max_edges
        return 'Edges: %d (Net density: %.2f%%)' % (e, density)
