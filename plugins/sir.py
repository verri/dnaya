# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from random import random, sample

from dnaya.api import DynamicalProcess


def validate_form(form):
    network = form['network']
    if network is None:
        raise ValueError('Select a network.')

    ni = form['# infected']
    if len(network) <= ni:
        raise ValueError('The number of infected must be less than the number'
                         'of vertices.')

REQUEST_FORM = {
    'network': {
        'type': 'network'
    },

    '# infected': {
        'type': 'integer',
        'min': 1,
        'max': 99999,
        'default': 1,
        'tip': 'Number of infected vertices at time 0.'
    },

    'γ': {
        'type': 'real',
        'default': 0.1,
        'step': 0.001,
        'precision': 3,
        'min': 0.001,
        'max': 0.999,
        'tip': 'Rate of recovery.'
    },

    'β': {
        'type': 'real',
        'default': 0.1,
        'step': 0.001,
        'precision': 3,
        'min': 0.001,
        'max': 0.999,
        'tip': 'Transmission rate.'
    },

    '!validate': validate_form
}


class SIRModel(DynamicalProcess):

    author = 'Verri'
    menu = 'Epidemic Model/SIR'
    description = 'SIR model without vital dynamics.'

    def __init__(self, parent=None):
        DynamicalProcess.__init__(self, parent)

    def request_form(self):
        return REQUEST_FORM

    def on_load(self, form):
        self.network = form['network']
        self.gamma = form['γ']
        self.beta = form['β']

        self.set_title('SIR Model (N={}, β={}, γ={})'.format(
            len(self.network), self.beta, self.gamma))

        self.initialize_network(form['# infected'])

    def initialize_network(self, ni):
        N = len(self.network)

        self.network['# infected'] = ni
        self.network['# susceptible'] = N - ni
        self.network['# recovered'] = 0

        infected = set(sample(xrange(N), ni))
        susceptible = set(xrange(N)) - infected

        for vid in infected:
            self.network[vid]['status'] = 'i'
            self.network[vid]['color'] = 'red'
        for vid in susceptible:
            self.network[vid]['status'] = 's'

    def on_update_vertex(self, vid):
        vertex = self.network[vid]

        if vertex['status'] != 'i':
            return

        ni = self.network['# infected']
        ns = self.network['# susceptible']

        for neighbor in vertex.neighbors('out'):
            if neighbor['status'] == 's' and random() < self.beta:
                neighbor['status'] = 'i'
                neighbor['color'] = 'red'
                ni += 1
                ns -= 1

        if random() < self.gamma:
            vertex['status'] = 'r'
            neighbor['color'] = 'cyan'
            self.network['# recovered'] += 1
            ni -= 1

        self.network['# infected'] = ni
        self.network['# susceptible'] = ns

    def on_update_epoch(self, epoch_id):
        ni = self.network['# infected']
        if ni == 0:
            raise StopIteration

        nr = self.network['# recovered']
        n = len(self.network)

        self.set_progress(int(100 * (nr + ni) / n))
