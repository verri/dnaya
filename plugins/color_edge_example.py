# -*- coding: utf-8 -*-
# Copyright © 2014, Filipe Verri and Paulo Urio.
#
# Distributed under the terms of the zlib License.  The full license is
# in the file LICENSE, distributed as part of this software.

from math import ceil
from random import randint

import numpy as np

from PyQt4.QtCore import QTimer

from dnaya.api import DynamicalProcess, Network


def validate_range(form):
    cmin = form['Cmin']
    cmax = form['Cmax']
    if cmin > cmax:
        msg = 'Range [Cmin, Cmax] = [{}, {}] is not valid.'.format(cmin, cmax)
        raise ValueError(msg)


REQUEST_FORM = {
    '!validate': validate_range,

    'N': {'type': 'integer',
          'default': 350,
          'min': 2,
          'tip': 'Total number of agents in the network.'},

    'K': {'type': 'integer',
          'default': 15,
          'min': 1,
          'tip': 'An agent <i>i</i> will possibly connect to vertices with indexes from i-K to i+K.'},

    'p': {'type': 'real',
          'step': 0.03,
          'default': 0.01,
          'min': 0.001,
          'tip': 'Probability of creating one link.'},

    'Cmin': {'type': 'integer',
             'default': 0,
             'min': 0,
             'max': 5,
             'tip': 'Minimum color index.'},

    'Cmax': {'type': 'integer',
             'default': 5,
             'min': 0,
             'max': 5,
             'tip': 'Maximum color index.'}
}


class ColorEdgeExampleModel(DynamicalProcess):
    author = 'Urio'
    menu = 'Examples/Colored edges'
    description = 'Erdős–Rényi random graph model with kN colored vertices.'

    def __init__(self, parent=None):
        DynamicalProcess.__init__(self, parent)

    def request_form(self):
        return REQUEST_FORM

    def on_load(self, form):
        self.assign_form(form)
        self.network = Network(self.N)
        self.set_title('Colored normal Erdős–Rényi (N={}, K={}, p={})'.format(
            self. N, self.K, self.p))
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_status)

    def on_update_vertex(self, vid):
        j = int(np.random.normal(vid, self.K))
        j = min(max(0, j), self.N - 1)
        self.last_vid = vid
        if (vid, j) not in self.network:
            self.network.add_edge(vid, j)
            self.set_color(vid, j)

    def set_color(self, ivid, jvid):
        edge = self.network[ivid, jvid]
        edge['color'] = randint(self.Cmin, self.Cmax)

    def on_update_epoch(self, epoch_id):
        if epoch_id == 0:
            self.timer.start(200)
        else:
            self.timer.stop()
            raise StopIteration

    def update_status(self):
        vid = self.last_vid
        self.set_progress(ceil(100 * (vid + 1) / self.N))
        self.set_status('Updating vertex {}.'.format(vid))
