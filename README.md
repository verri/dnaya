DNAYA
=====

DNAYA, pronounced naya, is a generic dynamical network analysis tool written in Python 2.

 - Write your own dynamical processes in a few lines of code
 - Visualize networks and dynamical processes in real-time 3D graphics
 - ...

The name D-N-A-Y-A, *the "D" is silent, hillbilly*, is a homage to my wife, however it may
also stand for **D**ynamical **N**etwork **A**nal**Y**sis via **A**gent-based simulation.

Dependencies
------------

 - Python 2.7.\*
 - VTK 6.\*
 - PyQt4 4.11.\*

Acknowledgements
----------------

This project is supported by **FAPESP** as part of the project *"High level data
classification based on complex network applied to invariant pattern recognition"*
(2013/25876-6).
